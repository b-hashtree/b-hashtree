
#include <vector>
#include <memory>

template <class Key,
          class T,
          class Hash = std::hash<Key>,
          class Pred = std::equal_to<Key>,
          class Alloc = std::allocator<std::pair<const Key, T>>>
class pair_seq_adapter
{

public:
    typedef Key key_type;  // the first template parameter (Key)
    typedef T mapped_type; // the second template parameter (T)
    typedef std::pair<const key_type, mapped_type> value_type;
    typedef Hash hasher;          // the third template parameter (Hash)  defaults to: hash<key_type>
    typedef Pred key_equal;       // the fourth template parameter (Pred) defaults to: equal_to<key_type>
    typedef Alloc allocator_type; // the fifth template parameter (Alloc) defaults to: allocator<value_type>
    typedef size_t size_type;

    pair_seq_adapter()
    {
    }

    mapped_type &at(const key_type &k)
    {
        for (auto &pair : bucket)
        {
            if ( cmp(pair.first, k) ) {
                return pair.second;
            }
        }
        throw std::out_of_range("Not found");
    }

    const mapped_type &at(const key_type &k) const
    {
        for (const auto &pair : bucket)
        {
            if ( cmp(pair.first, k) ) {
                return pair.second;
            }
        }
        throw std::out_of_range("Not found");
    }

    size_type count(const key_type &k) const
    {
        for (const auto &pair : bucket)
        {
            if ( cmp(pair.first, k) ) {
                return 1;
            }
        }
        return 0;
    }

    // Original declaration:
    // pair<iterator,bool> insert ( const value_type& val );
    // Change behaviour => returns whether it can insert val
    bool insert(const value_type &val)
    {
        bucket.push_back(val);
        return true;
    }

    size_type erase(const key_type &k)
    {
        auto it = bucket.begin();
        for ( ; it != bucket.end(); ++it )
        {
            if ( cmp(it->first, k) ) {
                break;
            }
        }
        if ( it != bucket.end() ) {
            bucket.erase(it);
        }
        return 1;
    }

    size_type size() const noexcept
    {
        return bucket.size();
    }

private:
    key_equal cmp;
    allocator_type alloc_;

    typedef std::vector<value_type, allocator_type> bucket_t;
    bucket_t bucket;
};