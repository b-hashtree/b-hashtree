#include <memory>
#include <vector>
#include <forward_list>

#define INITIAL_TABLE_SIZE 10

template<
		class Key,
		class T,
		class Hash = std::hash<Key>,
		class Pred = std::equal_to<Key>,
		class Alloc = std::allocator<std::pair<const Key, T>>>
class hashmap_py {

public:
	typedef Key key_type;
	typedef T mapped_type;
	typedef Hash hasher;
	typedef Pred key_equal;
	typedef Alloc allocator_type;
	typedef size_t size_type;
	typedef std::pair<const key_type, mapped_type> value_type;
	typedef std::forward_list<value_type> bucket;

	hashmap_py() {
		table_ptr = new std::vector<bucket>(INITIAL_TABLE_SIZE);
	}

	~hashmap_py() {

	}

	mapped_type &at(const key_type &key) {

	}

	bool insert(const value_type &value) {

	}

	size_type erase(const key_type &key) {

	}

	size_type count(const key_type &key) {

	}

	size_type size() const {
		return elements;
	}

protected:

private:
	std::vector<bucket> *table_ptr;
	int elements;
	int bucket_count;

	bool rehash_required() {

	}

	void rehash() {

	}

};