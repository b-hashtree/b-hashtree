
#include <vector>
#include <memory>

template < class Key,
           class T,
           class Hash = std::hash<Key>,
           class Pred = std::equal_to<Key>,
           class Alloc = std::allocator< std::pair<const Key,T> >
           >
class cuckoo_table {

public:

  cuckoo_table() {
    _size = 0;
    bucket_log_size = 9; // up to 512 * 2 elements
    for ( int selector = 0; selector <= 1; ++selector ) {
      bucket[selector].resize(1 << bucket_log_size);
    }
  }

  typedef Key key_type;     // the first template parameter (Key)
  typedef T mapped_type;    // the second template parameter (T)
  typedef std::pair<const key_type,mapped_type> value_type;
  typedef Hash hasher;            // the third template parameter (Hash)  defaults to: hash<key_type>
  typedef Pred key_equal;         // the fourth template parameter (Pred) defaults to: equal_to<key_type>
  typedef Alloc allocator_type;   // the fifth template parameter (Alloc) defaults to: allocator<value_type>
  typedef size_t size_type;

  // Memory management here!
  // typedef std::unique_ptr<value_type> ptr2value_type;
  struct custom_deleter {
      inline void operator() (value_type* pval) {
          allocator_type alloc_;
          pval->~value_type(); // call destructor, not using destroy since it has some compatibility issue between C++ versions
          alloc_.deallocate(pval, 1);
      }
  };
  typedef std::unique_ptr<value_type, custom_deleter> ptr2value_type;

  mapped_type& at ( const key_type& k )
  {
    for ( int selector = 0; selector <= 1; ++selector)
    {
      size_t h = internal_hash(k, selector);
      ptr2value_type& entry = bucket[selector][h];
      if (entry && cmp(entry->first, k) ) {
          return entry->second;
      }
    }
    throw std::out_of_range("Not found");
  }

  const mapped_type& at ( const key_type& k ) const
  {
    for ( int selector = 0; selector <= 1; ++selector)
    {
      size_t h = internal_hash(k, selector);
      const ptr2value_type& entry = bucket[selector][h];
      if (entry && cmp(entry->first, k) ) {
          return entry->second;
      }
    }
    throw std::out_of_range("Not found");
  }

  size_type count ( const key_type& k ) const {
    for ( int selector = 0; selector <= 1; ++selector)
    {
      size_t h = internal_hash(k, selector);
      const ptr2value_type& entry = bucket[selector][h];
      if (entry && cmp(entry->first, k) ) {
          return 1;
      }
    }
    return 0;
  }

  // Original declaration:
  // pair<iterator,bool> insert ( const value_type& val );
  // Change behaviour => returns whether it can insert val
  bool insert ( const value_type& val )
  {
#if LOG_LEVEL > 20
  cerr << "Inserting (" << val.first << ", " << val.second << ")" << endl;
#endif
    size_t h1[2];
    for ( int selector = 0; selector <= 1; ++selector)
    {
      h1[selector] = internal_hash(val.first, selector);
      const ptr2value_type& entry = bucket[selector][h1[selector]];
      if (entry && cmp(entry->first, val.first) ) {
          return false;
      }
    }
    // Memory management here!
    //ptr2value_type pval(new value_type(val));
    ptr2value_type pval(create_val(val));
    private_insert( std::move(pval) );
    return true;
  }

  size_type erase ( const key_type& k )
  {
    for ( int selector = 0; selector <= 1; ++selector)
    {
      size_t h = internal_hash(k, selector);
      ptr2value_type& entry = bucket[selector][h];
      if (entry && cmp(entry->first, k) ) {
          // Free slot
          --_size;
          entry.reset();
          return true;
      }
    }
    return false;
  }

  size_type size() const noexcept {
    return _size;
  }

private:

  size_t _size;

  hasher the_hash;
  key_equal cmp;
  allocator_type alloc_; 
  
  typedef std::vector< ptr2value_type > bucket_t;

  bucket_t bucket[2];

  size_t bucket_log_size;

  inline size_t internal_hash(const key_type& k, int selector) const {
    size_t h = the_hash(k);
    if ( selector ) {
      return h & ( (1 << bucket_log_size) - 1);
    } else {
      return ( h >> 8 ) & ( (1 << bucket_log_size) - 1);
    }
  }

  bool rehashing ( const value_type& val ) {
    // Reset size count
    _size = 0;
#if LOG_LEVEL > 8
    std::cerr << "Rehashing" << std::endl;
#endif
    bucket_t aux[2] = {
      bucket_t(std::move(bucket[0])),
      bucket_t(std::move(bucket[1]))
      };
    bucket_log_size += 1;

#if LOG_LEVEL > 8
    std::cerr << "Reconstructing buckets" << std::endl;
#endif
    for ( int selector = 0; selector <= 1; ++selector ) {
      // Reconstruct vector after moving
      bucket[selector] = bucket_t(1 << bucket_log_size);
    }

    for ( int selector = 0; selector <= 1; ++selector ) {
      for ( auto& previous_val : aux[selector] ) {
        if ( previous_val ) {
          private_insert( std::move(previous_val) );
        }
      }
    }
    return insert(val);
  }

  // This method insert a new element from an already
  // allocated and constructed value_type in a unique_ptr
  // by moving it to the buckets
  void private_insert ( ptr2value_type&& pval )
  {
#if LOG_LEVEL > 20
    cerr << "Not found, proceed to insertion" << endl;
#endif
    int selector = 0;
    unsigned count = 0;
    ptr2value_type ptr_val( std::move(pval) ) ;
    size_t h = internal_hash(ptr_val->first, selector);
    auto entry = bucket[selector].begin()+h;
    while ( entry->get() != nullptr && count < bucket_log_size ) {
#if LOG_LEVEL > 20
      cerr << "Trying to swap bucket entries: "
           << ptr_val->first << ", " << entry->get()->first << " ... ";
#endif
      entry->swap(ptr_val);
#if LOG_LEVEL > 20
      cerr << " Done" << endl;
#endif
      selector = !selector;
      h = internal_hash(ptr_val->first, selector);
      entry = bucket[selector].begin()+h;
      ++count;
    }

    if ( entry->get() != nullptr ) {
      rehashing(*ptr_val);
    } else {
      entry->swap(ptr_val);
      ++_size;
    }
  }

  inline value_type* create_val(const value_type& val) {
    value_type* ptr = alloc_.allocate(1);
#if __GNUC__ >= 11 
    std::construct_at(ptr, val);
#else
    alloc_.construct(ptr, val);
#endif
    return ptr;
  }
};
