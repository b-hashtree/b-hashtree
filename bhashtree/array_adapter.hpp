
#include <vector>
#include <memory>

template <class Key,
          class T,
          class Hash = std::hash<Key>,
          class Pred = std::equal_to<Key>,
          class Alloc = std::allocator<std::pair<const Key, T>>>
class array_adapter
{

public:
    typedef Key key_type;  // the first template parameter (Key)
    typedef T mapped_type; // the second template parameter (T)
    typedef std::pair<const key_type, mapped_type> value_type;
    typedef Hash hasher;          // the third template parameter (Hash)  defaults to: hash<key_type>
    typedef Pred key_equal;       // the fourth template parameter (Pred) defaults to: equal_to<key_type>
    typedef Alloc allocator_type; // the fifth template parameter (Alloc) defaults to: allocator<value_type>
    typedef size_t size_type;

    static inline unsigned shift = 0;

    array_adapter() 
    {
        bucket_size = 1024;
        bucket.resize(bucket_size);
    }

    mapped_type &at(const key_type &k)
    {
        key_type key = k >> shift;
        return bucket.at(key)->second;
    }

    const mapped_type &at(const key_type &k) const
    {
        key_type key = k >> shift;
        return bucket.at(key)->second;
    }

    size_type count(const key_type &k) const
    {
        key_type key = k >> shift;
        return bucket.at(key) == nullptr ? 0 : 1;
    }

    // Original declaration:
    // pair<iterator,bool> insert ( const value_type& val );
    // Change behaviour => returns whether it can insert val
    bool insert(const value_type &val)
    {
        key_type key = val.first >> shift;
        while ( key >= bucket_size ) 
        {
            if ( key > (1 << 22) ) 
            {
                std::cerr << "key: " << key << " is too big" << std::endl;
                std::cerr << "should not be possible to get here" << std::endl;
                exit(22);
            }
            bucket_size *= 2;
            bucket.resize(bucket_size);
        }
        bucket.at(key).reset(new value_type(val));
        ++_size;
        return true;
    }

    size_type erase(const key_type &k)
    {
        key_type key = k >> shift;
        bucket.at(key).reset();
        --_size;
        return false;
    }

    size_type size() const noexcept
    {
        return _size;
    }

private:
    size_t _size;
    typedef std::unique_ptr<value_type> ptr2value_type;
    typedef std::vector<ptr2value_type> bucket_t;
    bucket_t bucket;
    size_t bucket_size;
};