#include <vector>
#include <bitset>
#include <bit>
#include <stdexcept>
#include <list>

#define PREFIX_SIZE 6
#define BASE_SIZE 64

enum situations{EMPTY_ROOT, NOT_EMPTY, EMPTY_SUBTABLE};
enum operations{INSERT, SEARCH, ERASE};

template<
        class Key,
		class T,
		class Hash = std::hash<Key>,
		class Pred = std::equal_to<Key>,
		class Alloc = std::allocator<std::pair<const Key, T>>>
class hamt {

public:
	typedef Key key_type;
	typedef T mapped_type;
	typedef Hash hasher;
	typedef Pred key_equal;
	typedef Alloc allocator_type;
	typedef size_t size_type;
	typedef std::pair<const key_type, mapped_type> value_type;

protected:
	typedef std::pair<std::bitset<BASE_SIZE>, void *> table_entry_t;
	typedef std::vector<table_entry_t> table_t;
	typedef std::bitset<sizeof(size_type) * 8> bitset_t;

public:

	hamt() {
#ifdef DEBUGGING_COLLITIONS
		debug_collitions.open("collitions.txt");
		debug_collitions << "xxxxxx xxxxxx xxxxxx xxxxxx xxxxxx xxxxxx xxxxxx xxxxxx xxxxxx xxxxxx xxxx" << std::endl;
#endif
		root_table_ptr = new table_t(BASE_SIZE);
		locate_stack = new std::list<locate_stack_log>();
		elements = 0;
	}

	~hamt() {
#ifdef DEBUGGING_COLLITIONS
		debug_collitions.close();
#endif
	}

	mapped_type &at(const key_type &key) {
		locate_result target = locate(key, SEARCH);
		switch (target.situation) {
			case NOT_EMPTY:
			{
				return static_cast<value_type *>(target.entry_ptr->second)->second;
			}
			default:
			{
				throw std::out_of_range("Not found");
			}
		}
	}

	//bool insert(const key_type &key, const mapped_type &value) {
	bool insert(const value_type &value) {
		auto *new_pair_ptr = new value_type(value);
		locate_result locate_result = locate(value.first, INSERT);
		switch (locate_result.situation) {
			// Insertion into the root table. No new further tables must be allocated.
			case EMPTY_ROOT:
			{
				locate_result.entry_ptr->second = new_pair_ptr;
				break;
			}

			// Collision insertion.
			case NOT_EMPTY:
			{
				// Saves the old values pointer to reinsert afterwards
				auto *old_pair_ptr = static_cast<value_type *>(locate_result.entry_ptr->second);

				if (is_equals(old_pair_ptr->first, value.first)) {
					return false;
				}

				// Hashes the old key, iterates the production of tokens as many times as the new key has so both tokens are at the same position in the hash.
				bitset_t hashed_old_key = hash(static_cast<value_type *>(locate_result.entry_ptr->second)->first);
				for (int i = 0; i < locate_result.tokens_consumed + 1; ++i) { // tokens_consumed: numer of tokens at the left of the token that produced the collision.
					hashed_old_key = hashed_old_key << PREFIX_SIZE;
				}

#ifdef DEBUGGING_COLLITIONS
				std::string original_hash = static_cast<bitset_t>(hash(static_cast<value_type *>(locate_result.entry->second)->first)).to_string();
				std::string new_hash = static_cast<bitset_t>(hash(key)).to_string(), original_hash_formatted, new_hash_formatted;
				std::vector<std::string> vold, vnew; int counter = 0; std::string token_old, token_new;
				for (int i = 0; i < original_hash.length(); ++i) {
					++counter;
					token_old += original_hash[i];
					token_new += new_hash[i];
					if (counter == 6 || original_hash.length() - 1 == i) {
						vold.push_back(token_old);
						vnew.push_back(token_new);
						counter = 0;
						token_old = "";
						token_new = "";
					}
				}

				for (int i = 0; i < vold.size(); ++i) {
					if (vold.at(i) == vnew.at(i)) {
						original_hash_formatted += vold.at(i) + ' ';
						new_hash_formatted += vnew.at(i) + ' ';
					}
				}

				debug_collitions <<
				                 "OLD: \"" << static_cast<value_type *>(locate_result.entry->second)->first << "\", NEW: \"" << key << "\" at:" << std::endl <<
				                 original_hash_formatted << std::endl <<
				                 new_hash_formatted << std::endl << std::endl;
#endif

				auto *current_entry_ptr = locate_result.entry_ptr;
				int new_key_index, old_key_index = locate_result.entry_bitset_index;
				bool conti = true;
				while (conti) {

					old_key_index = (int) (hashed_old_key >> (sizeof(size_type) * 8 - PREFIX_SIZE)).to_ulong();
					hashed_old_key = hashed_old_key << PREFIX_SIZE;
					new_key_index = (int) (locate_result.hashed_key >> (sizeof(size_type) * 8 - PREFIX_SIZE)).to_ulong();
					locate_result.hashed_key = locate_result.hashed_key << PREFIX_SIZE;
					if (new_key_index == old_key_index) {
						auto *new_table_ptr = new table_t(1);
						current_entry_ptr->second = new_table_ptr;
						// Replaces the entry bitset with a real bitset with the correct bits set, points the entry's second value pointer to nullptr.
						current_entry_ptr->first.set(old_key_index);
						current_entry_ptr = &new_table_ptr->at(0);
					} else {
						auto *new_table_ptr = new table_t(2);
						current_entry_ptr->second = new_table_ptr;
						current_entry_ptr->first.set(old_key_index);
						current_entry_ptr->first.set(new_key_index);
						if (old_key_index < new_key_index) {
							new_table_ptr->at(0).second = old_pair_ptr;
							new_table_ptr->at(1).second = new_pair_ptr;
						} else {
							new_table_ptr->at(0).second = new_pair_ptr;
							new_table_ptr->at(1).second = old_pair_ptr;
						}
						conti = false;
					}
				}
				break;
			}

			// Inserton into a sub-hash table. The table must be resized.
			case EMPTY_SUBTABLE:
			{
				locate_result.entry_ptr->first.set(locate_result.entry_bitset_index);
				table_entry_t new_entry;
				new_entry.second = new_pair_ptr;
				auto &table = *locate_result.table_ptr;
				auto table_index = locate_result.table_index;
				if (table.size() > table_index && table.at(table_index).first.none() && table.at(table_index).second == nullptr) {
					table.at(1) = new_entry;
				} else {
					table.insert(table.begin() + table_index, new_entry);
				}
				break;
			}
		}
		++elements;
		return true;
	}

	size_type erase(const key_type &key) {
		locate_result target = locate(key, ERASE);
		switch (target.situation) {
			case NOT_EMPTY:
			{
				auto info = locate_stack->back();
				auto value_pair = static_cast<value_type *>(info.entry_ptr->second);

				if (is_equals(value_pair->first, key)) {
					auto table_ptr = info.table_ptr;
					auto entry_ptr = info.entry_ptr;
					auto entry_bitset_index = info.entry_bitset_index;
					auto table_index = info.table_index;

					delete value_pair;
					if (table_ptr->size() == 1 && table_ptr != root_table_ptr) {
						bool cont = true;
						while (cont) {
							delete table_ptr;
							locate_stack->pop_back();
							info = locate_stack->back();
							table_ptr = info.table_ptr;
							entry_ptr = info.entry_ptr;
							entry_bitset_index = info.entry_bitset_index;
							table_index = info.table_index;
							if (info.table_ptr->size() != 1) {
								cont = false;
							}
						}
					}
					if (table_ptr == root_table_ptr) {
						entry_ptr->first.set(entry_bitset_index, 0);
						entry_ptr->second = nullptr;
					} else {
						table_ptr->erase(table_ptr->begin() + table_index);
						locate_stack->pop_back();
						info = locate_stack->back();
						info.entry_ptr->first.set(entry_bitset_index, 0);
					}
					--elements;
					return true;
				}
			}

			default:
			{
				return false;
			}
		}
	}

	size_type count(const key_type &key) {
		locate_result target = locate(key, SEARCH);
		return (target.situation == NOT_EMPTY && is_equals(static_cast<value_type *>(target.entry_ptr->second)->first, key));
	}

	size_type size() const {
		return elements;
	}

private:
	table_t *root_table_ptr;
	size_type elements;
	hasher hash;
	key_equal is_equals;

	struct locate_stack_log {
		table_t *table_ptr;
		table_entry_t *entry_ptr;
		unsigned int table_index;
		unsigned int entry_bitset_index;
	};
	std::list<locate_stack_log> *locate_stack;

	struct locate_result {
		unsigned int situation;
		table_t *table_ptr;
		table_entry_t *entry_ptr;
		unsigned int table_index;
		unsigned int entry_bitset_index;
		unsigned int tokens_consumed;
		bitset_t hashed_key;

	};

	/*
	 * Returns the precise location where the key-value pair is supposed to be.
	 */
	locate_result locate(const key_type &key, unsigned int operation) {

		locate_stack->clear();

		bitset_t hashed_key = hash(key);
		unsigned int tokens_consumed = 0, table_index, entry_bitset_index = (hashed_key >> (sizeof(size_type) * 8 - PREFIX_SIZE)).to_ulong();
		table_t *current_table_ptr  = root_table_ptr;
		table_index = entry_bitset_index;
		table_entry_t *current_entry_ptr = &(*current_table_ptr)[table_index];

		hashed_key <<= PREFIX_SIZE;

		do {

			if (operation == ERASE && current_table_ptr != root_table_ptr) {
				locate_stack->push_back({current_table_ptr, current_entry_ptr, table_index, entry_bitset_index});
			}

			if ((*current_entry_ptr).first.none()) {
				if ((*current_entry_ptr).second == nullptr) {
					/* Empty slot (does not exist) in the root hash table */
					return {EMPTY_ROOT, nullptr, current_entry_ptr, 0, 0, 0, 0};
				} else {

					if (operation == ERASE && locate_stack->empty()) {
						locate_stack->push_back({current_table_ptr, current_entry_ptr, table_index, entry_bitset_index});
					}

					/* Non-empty slot (exists) */
					return {NOT_EMPTY, current_table_ptr, current_entry_ptr, table_index, entry_bitset_index, tokens_consumed, hashed_key};
				}
			} else {
				entry_bitset_index = (int) (hashed_key >> (sizeof(size_type) * 8 - PREFIX_SIZE)).to_ulong();
				hashed_key = hashed_key << PREFIX_SIZE;

				if (operation == ERASE && current_table_ptr == root_table_ptr) {
					locate_stack->push_back({current_table_ptr, current_entry_ptr, table_index, entry_bitset_index});
				}

				if (current_entry_ptr->first.test(entry_bitset_index)) {
					/* Continues searching */
					current_table_ptr = static_cast<table_t *>(current_entry_ptr->second);
					table_index = std::popcount((current_entry_ptr->first << (BASE_SIZE - entry_bitset_index - 1)).to_ulong()) - 1;
					current_entry_ptr = &current_table_ptr->at(table_index);
					++tokens_consumed;
				} else {
					/* Empty slot (does not exist) in a sub-hash table */
					auto next_table_ptr = static_cast<table_t *>(current_entry_ptr->second);
					table_index = std::popcount((current_entry_ptr->first << (BASE_SIZE - entry_bitset_index - 1)).to_ulong());
					return {EMPTY_SUBTABLE, next_table_ptr, current_entry_ptr, table_index, entry_bitset_index, 0, 0};
				}
			}
		} while (true);
	}
};

