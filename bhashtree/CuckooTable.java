
package bhashtree;

import java.util.*;

public class CuckooTable<K, V> implements Map<K, V> {

    Object[][] bucket = new Object[1024][2];
    int bucketLogSize = 10;
    private int _size = 0;

    @Override
    public V get(Object key) {
        int h0 = internalHash(key, bucketLogSize, 0);
        Entry<K, V> entry = internalGet(h0, 0);
        if (entry != null && entry.getKey().equals(key)) {
            return entry.getValue();
        }
        int h1 = internalHash(key, bucketLogSize, 1);
        entry = internalGet(h1, 1);
        if (entry != null && entry.getKey().equals(key)) {
            return entry.getValue();
        }
        return null;
    }

    @Override
    public V put(K key, V value) {
        Entry<K, V> entry = new AbstractMap.SimpleEntry<>(key, value);
        return internalInsert(entry);
    }

    @Override
    public V remove(Object key) {
        for ( int i = 0; i < 2; ++i ) {
            int h = internalHash(key, bucketLogSize, i);
            Entry<K, V> entry = internalGet(h, i);
            if (entry != null && entry.getKey().equals(key)) {
                bucket[h][i] = null;
                --_size;
                return entry.getValue();
            }    
        }
        return null;        
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean containsKey(Object key) {
        int h0 = internalHash(key, bucketLogSize, 0);
        Entry<K, V> entry = internalGet(h0, 0);
        if (entry != null && entry.getKey().equals(key)) {
            return true;
        }
        int h1 = internalHash(key, bucketLogSize, 1);
        entry = internalGet(h1, 1);
        if (entry != null && entry.getKey().equals(key)) {
            return true;
        }
        return false;
    }

    @Override
    public boolean containsValue(Object arg0) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isEmpty() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Set<K> keySet() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> arg0) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int size() {
        return _size;
    }

    @Override
    public Collection<V> values() {
        throw new UnsupportedOperationException();
    }

    static private int internalHash(Object k, int bucketLogSize, int index2) {
        int h = k.hashCode();
        if (index2 == 0) {
            int result = h;
            return result & ((1 << bucketLogSize) - 1); // Only the mask to bucketLogSize
        } else {
            int result = h >> 8;
            return result & ((1 << bucketLogSize) - 1); // Only the mask to bucketLogSize
        }
    }

    private Entry<K, V> internalGet(int h, int index2) {
        @SuppressWarnings("unchecked")
        Entry<K, V> entry = (Entry<K, V>) bucket[h][index2];
        return entry;
    }

    @SuppressWarnings("unchecked")
    private V internalInsert(Entry<K, V> entry) {
        int selector = 0;
        int count = 0;
        int h = internalHash(entry.getKey(), bucketLogSize, selector);
        Entry<K, V> oldEntry = (Entry<K, V>) bucket[h][selector];

        while (oldEntry != null && !entry.getKey().equals(oldEntry.getKey()) && count < bucketLogSize) {
            bucket[h][selector] = entry;
            entry = (Entry<K, V>) oldEntry;
            selector = selector == 0 ? 1 : 0;
            h = internalHash(entry.getKey(), bucketLogSize, selector);
            oldEntry = (Entry<K, V>) bucket[h][selector];
            ++count;
        }

        if (oldEntry == null) {
            ++_size;
            bucket[h][selector] = entry;
            return null;
        } else if (entry.getKey().equals(oldEntry.getKey())) {
            V oldValue = oldEntry.getValue();
            oldEntry.setValue(entry.getValue());
            return oldValue;
        } else { // rehashing
            return rehashing(entry);
        }
    }

    private V rehashing(Entry<K, V> lastPut) {
        // Resize buckets and put all entries again including lastPut
        Object[][] aux = bucket;
        bucketLogSize += 1;
        // System.err.println("Rehashing to bucketLogSize: " + bucketLogSize);

        bucket = new Object[1 << bucketLogSize][2];

        for (int i = 0; i < aux.length; ++i) {
            for (int j = 0; j < 2; ++j) {
                Object obj = aux[i][j];
                if (obj != null) {
                    @SuppressWarnings("unchecked")
                    Entry<K, V> entry = (Entry<K, V>) obj;
                    internalInsert(entry);
                }
            }
        }
        // Insert argument from last call
        // System.err.println("lastPut " + lastPut.key);
        return internalInsert(lastPut);
    }
}
