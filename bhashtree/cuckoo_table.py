class CuckooTable:


    def __init__(self):    
        self.bucket=[[None for x in range(2)] for y in range(1024)]
        self.bucketLogSize = 10
        self._size = 0


    def  __getitem__(self,key):
        h0= self.internalHash(key,self.bucketLogSize,0)
        entry = self.internalGet(h0,0)
        if(entry!=None and entry[0]==key):
            return entry[1]
        h1 = self.internalHash(key,self.bucketLogSize,1)
        entry = self.internalGet(h1,1)
        if(entry!=None and entry[0]==key):
            return entry[1]
        return None
    
    def __setitem__(self, key, value, /):
        entry = (key,value)
        self._size+=1
        return self.internalInsert(entry)
    
    def pop(self, key, /):
        for i in range(0,2):
            h=self.internalHash(key, self.bucketLogSize,i)
            entry = self.internalGet(h,i)
            if(entry!=None and entry[0]==key):
                self.bucket[h][i]=None
                self._size-=1
                return entry[1]
        return None


    def __len__(self, /):
        return self._size

    def internalHash(self,k, bucketLogSize, index2):
        h=hash(k)
        if (index2 == 0):
            result = h 
            return result & ((1 << bucketLogSize) - 1)
        else:
            result = h >> 8
            return result & ((1 << bucketLogSize) - 1)
        
    def internalGet(self,h,index2):
        entry=self.bucket[h][index2]
        return entry
    
    def internalInsert(self,entry):
        selector = 0
        count = 0
        h = self.internalHash(entry[0], self.bucketLogSize, selector)
        oldEntry = self.bucket[h][selector]

        while (oldEntry != None and not(entry[0]==oldEntry[0]) and count < self.bucketLogSize):
            self.bucket[h][selector]=entry
            entry = oldEntry
            if(selector==0):
                selector=1
            else:
                selector=0
            
            h = self.internalHash(entry[0], self.bucketLogSize, selector)
            oldEntry = self.bucket[h][selector]
            count+=1
        

        if (oldEntry == None):
            self.bucket[h][selector] = entry
            return None
        elif (entry[0]==oldEntry[0]):
            oldValue = oldEntry[1]
            oldEntry=(oldEntry[0],entry[1])
            self.bucket[h][selector] = oldEntry
            return oldValue
        else:
            return self.rehashing(entry)
    
    def rehashing(self,lastPut):
        aux = self.bucket
        self.bucketLogSize+=1

        self.bucket = [[None for x in range(2)] for y in range(2<<self.bucketLogSize)]

        for i in range(0,len(aux)):
            for j in range(0,2):
                obj = aux[i][j]
                if (obj != None):
                    entry = obj
                    self.internalInsert(entry)
        
        return self.internalInsert(lastPut)


