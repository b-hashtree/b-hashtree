#include <memory>
#include <vector>

#define INITIAL_TABLE_SIZE 10
#define MAX_LOAD_FACTOR 1

template<
		class Key,
		class T,
		class Hash = std::hash<Key>,
		class Pred = std::equal_to<Key>,
		class Alloc = std::allocator<std::pair<const Key, T>>>
class bst {
public:
	typedef Key key_type;
	typedef T mapped_type;
	typedef Hash hasher;
	typedef Pred key_equal;
	typedef Alloc allocator_type;
	typedef std::pair<const key_type, mapped_type> value_type;

	bst() {
		root_ptr = nullptr;
	}

	mapped_type &get(const key_type &key) {
		bst_node *prev_node_ptr = nullptr;
		bst_node *node_ptr = root_ptr;
		while (node_ptr != nullptr) {
			if (is_equals(key, node_ptr->key)) {
				return node_ptr->value;
			} else if (key < node_ptr->key) {
				node_ptr = node_ptr->left_child;
			} else {
				node_ptr = node_ptr->right_child;
			}
		}
		throw std::out_of_range("Not found");
	}

	/*
	 * Returns true if the insertion succeeded and false if the key is already inserted (no duplicated allowed).
	 */
	bool insert(const value_type &pair) {
		key_type key = pair.first;
		bst_node *prev_node_ptr;
		bst_node *node_ptr = root_ptr;

		while (node_ptr != nullptr) {
			prev_node_ptr = node_ptr;
			if (is_equals(key, node_ptr->key)) {
				return false;
			} else if (key < node_ptr->key) {
				node_ptr = node_ptr->left_child;
			} else {
				node_ptr = node_ptr->right_child;
			}
		}
		if (node_ptr == root_ptr) {
			root_ptr = new bst_node{key, pair.second, nullptr, nullptr, nullptr};
		} else if (key < prev_node_ptr->key) {
			prev_node_ptr->left_child = new bst_node{key, pair.second, prev_node_ptr, nullptr, nullptr};
		} else {
			prev_node_ptr->right_child = new bst_node{key, pair.second, prev_node_ptr, nullptr, nullptr};
		}
		++elements;
		return true;
	}

	/*
	 * Returns true if the erase succeeded and false if the key does not exist.
	 */
	bool erase(const key_type &key) {
		bst_node *node_ptr = root_ptr;

		while (node_ptr != nullptr) {
			if (is_equals(key, node_ptr->key)) {
				if (node_ptr->left_child == nullptr) {
					transplant(node_ptr, node_ptr->right_child, true);
				} else if (node_ptr->right_child == nullptr) {
					transplant(node_ptr, node_ptr->left_child, true);
				} else {
					bst_node *min = minimum(node_ptr->right_child);
					if (min->parent != node_ptr) {
						transplant(min, min->right_child, false);
						min->right_child = node_ptr->right_child;
						min->right_child->parent = min;
					}
					transplant(node_ptr, min, true);
					min->left_child = node_ptr->left_child;
					min->left_child->parent = min;
				}
				--elements;
				return true;
			} else if (key < node_ptr->key) {
				node_ptr = node_ptr->left_child;
			} else {
				node_ptr = node_ptr->right_child;
			}
		}
		return false;
	}

	std::vector<value_type> *content_to_list() {
		auto *result = new std::vector<value_type>;
		traversal_mapping(result, root_ptr);
		return result;
	}

private:
	struct bst_node {
		key_type key;
		mapped_type value;
		bst_node *parent;
		bst_node *left_child;
		bst_node *right_child;
	};

	bst_node *root_ptr;
	unsigned int elements;
	key_equal is_equals;

	bst_node *minimum(bst_node *root) {
		bst_node *node_ptr = root;
		while (node_ptr->left_child != nullptr) {
			node_ptr = node_ptr->left_child;
		}
		return node_ptr;
	}

	void transplant(bst_node *original, bst_node *replacement, bool destroy) {
		if (original->parent == nullptr) {
			root_ptr = replacement;
		} else {
			if (original->parent->left_child != nullptr && is_equals(original->key, original->parent->left_child->key)) {
				original->parent->left_child = replacement;
			} else {
				original->parent->right_child = replacement;
			}
		}
		if (replacement != nullptr) {
			replacement->parent = original->parent;
		}
		if (destroy) {
			delete original;
		};
	}

	// Maps the whole tree into a list using a recursive Postorder tree walk algorithm
	void traversal_mapping(std::vector<value_type> *list, const bst_node *node_ptr) {
		if (node_ptr != nullptr) {
			traversal_mapping(list, node_ptr->left_child);
			traversal_mapping(list, node_ptr->right_child);
			value_type value(node_ptr->key, node_ptr->value);
			list->push_back(value);
		}
	}
};

template<
		class Key,
		class T,
		class Hash = std::hash<Key>,
		class Pred = std::equal_to<Key>,
		class Alloc = std::allocator<std::pair<const Key, T>>>
class hashmap_tree {

public:
	typedef Key key_type;
	typedef T mapped_type;
	typedef Hash hasher;
	typedef Pred key_equal;
	typedef Alloc allocator_type;
	typedef size_t size_type;
	typedef std::pair<const key_type, mapped_type> value_type;

	typedef bst<key_type, mapped_type> tree_type;
	typedef std::vector<tree_type> table_type;

	hashmap_tree() {
		table_ptr = new table_type(INITIAL_TABLE_SIZE);
		elements = 0;
		bucket_count = INITIAL_TABLE_SIZE;
	}

	~hashmap_tree() {
		delete table_ptr;
	}

	mapped_type &at(const key_type &key) {
		return table_ptr->at(hash_key(key)).get(key);
	}

	bool insert(const value_type &pair) {
		value_type v(pair.first, pair.second);
		if (table_ptr->at(hash_key(pair.first)).insert(v)) {
			++elements;
			if (rehash_required()) {
				rehash();
			}
			return true;
		}
		return false;
	}

	size_type erase(const key_type &key) {
		if (table_ptr->at(hash_key(key)).erase(key)) {
			--elements;
			return true;
		}
		return false;
	}

	size_type count(const key_type &key) {
		try {
			table_ptr->at(hash_key(key)).get(key);
			return true;
		} catch (std::out_of_range &e) {
			return false;
		}
	}

	size_type size() const {
		return elements;
	}

protected:

private:
	table_type *table_ptr;
	unsigned int elements;
	unsigned int bucket_count;
	hasher hash;

	unsigned long hash_key(const key_type &key) {
		return hash(key) % bucket_count;
	}

	bool rehash_required() {
		return (double) elements / bucket_count >= MAX_LOAD_FACTOR;
	}

	void rehash() {
		auto old_table_ptr = table_ptr;
		elements = 0;
		bucket_count *= 2;
		table_ptr = new table_type(bucket_count);
		for (bst tree : *old_table_ptr) {
			auto *a = tree.content_to_list();
			for (value_type pair : *a) {
				insert(pair);
			}
		}
		delete old_table_ptr;
	}
};