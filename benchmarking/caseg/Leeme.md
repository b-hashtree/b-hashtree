
# Caso G

* Vamos a probar a incrementar el uso de memoria por elemento
para ver si vemos que la velocidad cambia de tendencia
al llegar al límite de memoria del cache L3

* Vamos a probar un mapa con claves que son string, pero cuyo 
contenido lo vamos a generar como números aleatorios, dos
números aleatorios por clave. 

* Para generar números aleatorios para las claves usaremos
un rango hasta 32*size, luego vamos a comprobar que no estén repetidos
(comprobando que no están en el map), e insertamos hasta llegar a tamaño deseado.

* Vamos a meter como valor 8 números aleatorios de tipo double en un vector. 
