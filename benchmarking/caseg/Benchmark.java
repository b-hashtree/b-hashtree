/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
                      Daniel García-García  (UPM)
 *                    Pablo García-Hernández (UPM)
 *
 * This file is part of B-hashtree Library.
 *
 *     B-hashtree Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     B-hashtree Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with B-hashtree Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

import java.util.*;

public class Benchmark {

    final static Random random = new Random(30);

    public static ArrayList<Double> evaluateString(Map<String, Vector8> map, int size, int numberOfOp) {
        ArrayList<Double> ellapsed = new ArrayList<>();
        Random random = new Random(30);

        ellapsed.add(System.nanoTime() / 1e6); // Take time in milliSeconds

        String[] numbers = new String[size];
        int i = 0;
        while (i < size) {
            String key = Integer.toString(random.nextInt(32 * size));
            if (!map.containsKey(key)) {
                numbers[i] = key;
                map.put(key, new Vector8(random));
                ++i;
            }
        }

        ellapsed.add(System.nanoTime() / 1e6); // Take time in milliSeconds

        double sum = 0;
        for (int j = 1; j <= numberOfOp; ++j) {
            String key = numbers[j % size];
            Vector8 mapped = map.get(key);
            if (j % size == 0) {
                System.err.println("Average: " + (sum / 8 / size));
                sum = 0;
            } else {
                for (int k = 0; k < 8; ++k) {
                    sum += mapped.v[k];
                }
            }
        }
        ellapsed.add(System.nanoTime() / 1e6); // Take time in milliSeconds

        return ellapsed;
    }

    public static void main(String[] args) {
        int logSize = Integer.parseInt(args[1]);
        int size = 1 << logSize; // 2 ** logSize
        int numberOfOp = Integer.parseInt(args[2]);
        ArrayList<Double> ellapsed = null;

        Map<String, Vector8> map;
        if (args[0].equalsIgnoreCase("Hashtable")) {
            map = new Hashtable<>();
            ellapsed = evaluateString(map, size, numberOfOp);
        } else if (args[0].equalsIgnoreCase("HashMap")) {
            map = new HashMap<>();
            ellapsed = evaluateString(map, size, numberOfOp);
        } else if (args[0].equalsIgnoreCase("CuckooTable")) {
            map = new bhashtree.CuckooTable<>();
            ellapsed = evaluateString(map, size, numberOfOp);
        }

        for (int i = 1; i < ellapsed.size(); ++i) {
            System.out.print(String.format(Locale.US, "%.3f\t", ellapsed.get(i) - ellapsed.get(i - 1)));
        }
        int[] numberofIterations = { size, numberOfOp };
        for (int i = 1; i < ellapsed.size(); ++i) {
            double speed = numberofIterations[i - 1] / (ellapsed.get(i) - ellapsed.get(i - 1)); // number of iterations
                                                                                                // per **milli** second
            System.out.print(String.format(Locale.US, "%.3f\t", speed));
        }
        System.out.println();
    }

}

class Vector8 {

    public double[] v;

    public Vector8(Random random) {
        v = new double[8];
        for (int i = 0; i < v.length; ++i) {
            v[i] = random.nextDouble();
        }
    }
}
