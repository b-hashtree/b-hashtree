""" ***************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
                      Daniel García-García  (UPM)
 *                    Pablo García-Hernández (UPM)
 *
 * This file is part of B-hashtree Library.
 *
 *     B-hashtree Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     B-hashtree Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with B-hashtree Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/ """

import time
import sys
import random
import os 

if __name__ == "__main__":
    dir_path = os.path.dirname(os.path.realpath(__file__))
    if os.getcwd() == dir_path:
        sys.path.append("../..")
    else:
        print("Error: this script is designed to be executed in his own folder as working dir")
        sys.exit(22)

from bhashtree.cuckoo_table import CuckooTable

class Vector8: 
    def __init__(self):
        self.v = [ random.uniform(0,1) for i in range(0,8) ]
    
    def __iter__(self):
        return self.v.__iter__()

class CaseD:

    @staticmethod
    def evaluate(map, size, numberOfOp):
        ellapsed = [ time.time_ns()/1e6 ] # milliseconds

        numbers = []
        i = 0; 
        while i < size: 
            key = str(random.randint(0,32*size))
            if not key in map: 
                numbers.append(key)
                map[key] = Vector8()
                i += 1

        ellapsed.append(time.time_ns()/1e6)

        sum = 0
        for i in range(0,numberOfOp):
            key = numbers[i % size]
            v8 = map[key]
            if i == size:
                sys.stderr.write("Sum: " + str(sum / 8 / size) + "\n")
                sum = 0
            else: 
                for number in v8: sum += number

        ellapsed.append(time.time_ns()/1e6)

        return ellapsed

    @staticmethod
    def main():
        logSize = int(sys.argv[1])
        size = 1 << logSize
        numberOfOp = int(sys.argv[2])
        which = str(sys.argv[3])
        if which == "dict": map = {}
        elif which == "cuckoo-table": map = CuckooTable()
        else:
            sys.stderr.write("Unknown class: " + which + "\n")
            sys.exit(22)

        ellapsed = CaseD.evaluate(map,size,numberOfOp)

        for i in range(1,len(ellapsed)):
            print(ellapsed[i]-ellapsed[i-1], end='\t')
        
        numberofIterations = [ size, numberOfOp] 
        for i in range(1,len(ellapsed)):
            speed = numberofIterations[i-1] / (ellapsed[i]-ellapsed[i-1])
            print(speed, end='\t')
        print()

if __name__ == "__main__":
    CaseD.main()
