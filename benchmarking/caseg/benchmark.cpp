/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
                      Daniel García-García  (UPM)
 *                    Pablo García-Hernández (UPM)
 *
 * This file is part of B-hashtree Library.
 *
 *     B-hashtree Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     B-hashtree Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with B-hashtree Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include <string>
#include <iostream>
#include <sstream>
#include <random>
#include <algorithm>
#include <iterator>
#include <chrono>
#include <cstdio>
#include <unordered_map>
#include <map>
#include <tuple>

#ifdef DEBUGGING_OUTPUTS
#define LOG_LEVEL 15
#else
#define LOG_LEVEL 0
#endif

#include "../shuffle_array.hpp"

#include "bhashtree/cuckoo_table.hpp"
#include "bhashtree/hamt.hpp"
#include "bhashtree/hashmap_tree.hpp"
#include "bhashtree/pair_seq_adapter.hpp"

using namespace std;

typedef pair<chrono::time_point<chrono::high_resolution_clock>, string> checkpoint_t;
typedef vector<checkpoint_t> checkpoints_t;

typedef std::array<double, 8> Vector8;

class GenerateVector8 {
public:
  static Vector8 make(uniform_real_distribution<double>& real_rand, default_random_engine& random_eng) {
    Vector8 v;
    for ( auto& val : v ) {
      val = real_rand(random_eng);
    }
    return v;
  }
  
};

template <class table_t>
double case_g(size_t size, size_t numberOfOp, checkpoints_t &ellapsed)
{
  vector<string> numbers(size, "");
  table_t table;
  unsigned seed = 30;
  default_random_engine random_eng(seed);
  uniform_int_distribution<unsigned> uniform_dist(0, 32 * size);
  uniform_real_distribution<double> real_rand(0, 1);

  ellapsed.push_back(checkpoint_t(chrono::high_resolution_clock::now(), "Start")); // Taking time

  // Build Map (checking duplicate in this benchmark)
  unsigned i = 0;
  while (i < size)
  {
    typename table_t::key_type key = to_string(uniform_dist(random_eng)) + to_string(uniform_dist(random_eng));
    if (table.count(key) == 0)
    {
      table.insert(typename table_t::value_type(key, GenerateVector8::make(real_rand,random_eng)));
      numbers[i] = key;
      ++i;
    }
  }

  ellapsed.push_back(checkpoint_t(chrono::high_resolution_clock::now(), "Build Map")); // Taking time

  double sum = 0;
  for (int i = 0; i < numberOfOp; ++i)
  {
    const auto &key = numbers[i % size];
    const auto &mapped = table.at(key);
    if (i == size) {
	    cerr << "Sum: " << sum / 8 / size << endl;
	    sum = 0;
    } else {
      for ( const auto& val : mapped ) {
        sum += val;
      }
		}
  }
  ellapsed.push_back(checkpoint_t(chrono::high_resolution_clock::now(), "Lookups")); // Taking time
  return sum;
}

int main(int argc, char **argv)
{
  int exponent = 5;
  char which = 'c';
  size_t numberOfOp = 100;

  if (argc >= 2)
  {
    istringstream arg1(argv[1]);
    arg1 >> exponent;
  }

  if (argc >= 3)
  {
    istringstream arg2(argv[2]);
    arg2 >> which;
  }

  if (argc >= 4)
  {
    istringstream arg2(argv[3]);
    arg2 >> numberOfOp;
  }

  size_t n_erase, n_get, n_insert;
  checkpoints_t time_ellapseds;
  size_t size = 1 << exponent; // 2 ** exponent
  double returning;

  switch (which)
  {
  case 'm':
    returning = case_g<map<string, Vector8>>(size, numberOfOp, time_ellapseds);
    break;
  case 'u':
    returning = case_g<unordered_map<string, Vector8>>(size, numberOfOp, time_ellapseds);
    break;
  case 'b':
    returning = case_g<hashmap_tree<string, Vector8>>(size, numberOfOp, time_ellapseds);
    break;
  case 'h':
    returning = case_g<hamt<string, Vector8>>(size, numberOfOp, time_ellapseds);
    break;
  case 'c':
    returning = case_g<cuckoo_table<string, Vector8>>(size, numberOfOp, time_ellapseds);
    break;
  case 's':
    returning = case_g<pair_seq_adapter<string, Vector8>>(size, numberOfOp, time_ellapseds);
    break;
  default:
    cerr << "Can't find class id for`" << which << "'" << endl;
    exit(21);
    break;
  }

  for (int i = 1; i < time_ellapseds.size(); ++i)
  {
    auto t = chrono::duration_cast<chrono::duration<double>>(time_ellapseds[i].first - time_ellapseds[i - 1].first); // seconds
    printf("%.3f\t", t.count() * 1e3);
  }
  size_t numberofIterations[] = { size, numberOfOp };
  for (size_t i = 1; i < time_ellapseds.size(); ++i)
  {
    auto t = chrono::duration_cast<chrono::duration<double>>(time_ellapseds[i].first - time_ellapseds[i - 1].first); // seconds
    double speed = numberofIterations[i-1] / t.count() / 1e3; // number of iterations per **milli** second
    printf("%.3f\t", speed);
  }
  printf("\n");
  return 0;
}
