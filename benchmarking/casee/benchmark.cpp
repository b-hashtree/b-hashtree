/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
                      Daniel García-García  (UPM)
 *                    Pablo García-Hernández (UPM)
 *
 * This file is part of B-hashtree Library.
 *
 *     B-hashtree Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     B-hashtree Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with B-hashtree Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include <string>
#include <iostream>
#include <sstream>
#include <random>
#include <algorithm>
#include <iterator>
#include <chrono>
#include <cstdio>
#include <unordered_map>
#include <map>
#include <tuple>
#include <math.h>

#ifdef DEBUGGING_OUTPUTS
#define LOG_LEVEL 15
#else
#define LOG_LEVEL 0
#endif

#include "../shuffle_array.hpp"

#include "bhashtree/array_adapter.hpp"
#include "bhashtree/cuckoo_table.hpp"
#include "bhashtree/hamt.hpp"

using namespace std;

#define PRECISION 1E-10

const int SECTORS = 24;
const int ANGLE_INC_DEGREES = 360 / SECTORS;
const double RADIUS = 1.1;
const double SLOPE = 0.017;

class Vector3
{
public:
  double x, y, z;
};

ostream& operator<<(ostream& os, const Vector3& v) {
  os << v.x << "\t" << v.y << "\t" << v.z;
  return os;
}

struct EqualVector3
{
  constexpr bool operator()(const Vector3 &lhs, const Vector3 &rhs) const
  {
    return fabs(lhs.x - rhs.x) < PRECISION && fabs(lhs.y - rhs.y) < PRECISION && fabs(lhs.z - rhs.z) < PRECISION;
  }
};

struct LessVector3
{
  constexpr bool operator()( const Vector3& lhs, const Vector3& rhs ) const
  {
    return (lhs.z < rhs.z) || (lhs.z == rhs.z) && (lhs.x < rhs.x) || (lhs.z == rhs.z) && (lhs.x == rhs.x) && (lhs.y < rhs.y);
  }
};

struct HashVector3
{
  /* Note: Vector3 hash computed (probably) as in Java */
  size_t operator()(const Vector3 &v) const
  {
    static auto hasher = hash<double>();
    size_t hashCode = 1;
    hashCode = 31 * hashCode + hasher(v.x);
    hashCode = 31 * hashCode + hasher(v.y);
    hashCode = 31 * hashCode + hasher(v.z);
    return hashCode;
  }
};

class AnglePass
{
public:
  double alpha;
  int index;
  int accessed;

  AnglePass() : index(0), alpha(0.0), accessed(0) {  }

  AnglePass(int i) : index(i), alpha((i % SECTORS) * ANGLE_INC_DEGREES * M_PI / 180), accessed(0) { }

  static Vector3 calculate(const AnglePass &anglePass)
  {
    Vector3 v3;
    v3.x = RADIUS * sin(anglePass.alpha);
    v3.y = RADIUS * cos(anglePass.alpha);
    v3.z = SLOPE * anglePass.index;
    return v3;
  }
};

ostream& operator<<(ostream& os, const AnglePass& a) {
  os << a.index << "\t" << a.alpha << "\t" << a.accessed;
  return os;
}

typedef pair<chrono::time_point<chrono::high_resolution_clock>, string> checkpoint_t;
typedef vector<checkpoint_t> checkpoints_t;

template <class table_t>
void case_e(size_t size, size_t numberOfOp, checkpoints_t &ellapsed)
{
  table_t map;
  unsigned seed = 30;
  default_random_engine random_eng(seed);
  uniform_int_distribution<unsigned> uniform_dist(0, size-1);

  ellapsed.push_back(checkpoint_t(chrono::high_resolution_clock::now(), "Start")); // Taking time

  // Build Map (checking duplicate in this benchmark)
  /*
  * ANGLE_INC_DEGREES = 360 / SECTORS
  * alpha = (i % SECTORS) * ANGLE_INC_DEGREES * PI / 180
  * x = RADIUS * sin(alpha)
  * y = RADIUS * cos(alpha)
  * z = SLOPE * i
  */

  vector<AnglePass> anglePasses(size);
  vector<Vector3> points(size);

  for (size_t i = 0; i < size; ++i)
  {
    AnglePass anglePass = AnglePass(i);
    Vector3 key = AnglePass::calculate(anglePass);
    anglePasses[i] = anglePass;
    points[i] = key;
    map.insert(typename table_t::value_type(key, anglePass));
  }

  ellapsed.push_back(checkpoint_t(chrono::high_resolution_clock::now(), "Build Map")); // Taking time
  EqualVector3 equal;

#if LOG_LEVEL >= 10  
  cerr << "size: " << map.size() << endl;
#endif

#if LOG_LEVEL >= 222  
  for ( const auto& pair: map ) { // Custom classes don't have iterators...
    cerr << pair.first << " -> " << pair.second << endl;
  }
#endif

  for (size_t j = 0; j < numberOfOp; ++j)
  {
    size_t index = uniform_dist(random_eng);
    const Vector3 &key = points[index];
    try {
      AnglePass &mapped = map.at(key);
      mapped.accessed += 1;
      Vector3 checking = AnglePass::calculate(mapped);
      if (!equal(key, checking))
      {
        cerr << "Error when ckecking Vector3. Should not be possible to get here" << endl;
        exit(21);
      }
    } 
    catch ( const exception& e) {
      cerr << "Could not access to: " << key << " from index: " << index << endl;
      cerr << e.what() << endl;
      exit(22);
    }
  }
  ellapsed.push_back(checkpoint_t(chrono::high_resolution_clock::now(), "Lookups")); // Taking time

  int totalAccessed = 0;
  for (size_t i = 0; i < size; ++i)
  {
    const Vector3 &key = points[i];
    const AnglePass &mapped = map.at(key);
    totalAccessed += mapped.accessed;
  }

  if (totalAccessed != numberOfOp)
  {
    cerr << "Error when ckecking number of operations. Should not be possible to get here" << endl;
    exit(23);
  }
}

int main(int argc, char **argv)
{
  int exponent = 5;
  char which = 'c';
  size_t numberOfOp = 100;

  if (argc >= 2)
  {
    istringstream arg1(argv[1]);
    arg1 >> exponent;
  }

  if (argc >= 3)
  {
    istringstream arg2(argv[2]);
    arg2 >> which;
  }

  if (argc >= 4)
  {
    istringstream arg2(argv[3]);
    arg2 >> numberOfOp;
  }

  size_t n_erase, n_get, n_insert;
  checkpoints_t time_ellapseds;
  size_t size = 1 << exponent; // 2 ** exponent
  double returning;

  switch (which)
  {
  case 'm':
    case_e<map<Vector3, AnglePass, LessVector3>>(size, numberOfOp, time_ellapseds);
    break;
  case 'u':
    case_e<unordered_map<Vector3, AnglePass, HashVector3, EqualVector3>>(size, numberOfOp, time_ellapseds);
    break;
  case 'h':
    case_e<hamt<Vector3, AnglePass, HashVector3, EqualVector3>>(size, numberOfOp, time_ellapseds);
    break;
  case 'c':
    case_e<cuckoo_table<Vector3, AnglePass, HashVector3, EqualVector3>>(size, numberOfOp, time_ellapseds);
    break;
  default:
    cerr << "Can't find class id for`" << which << "'" << endl;
    exit(21);
    break;
  }

  for (int i = 1; i < time_ellapseds.size(); ++i)
  {
    auto t = chrono::duration_cast<chrono::duration<double>>(time_ellapseds[i].first - time_ellapseds[i - 1].first); // seconds
    printf("%.3f\t", t.count() * 1e3);
  }
  size_t numberofIterations[] = { size, numberOfOp };
  for (size_t i = 1; i < time_ellapseds.size(); ++i)
  {
    auto t = chrono::duration_cast<chrono::duration<double>>(time_ellapseds[i].first - time_ellapseds[i - 1].first); // seconds
    double speed = numberofIterations[i-1] / t.count() / 1e3; // number of iterations per **milli** second
    printf("%.3f\t", speed);
  }
  printf("\n");
  return 0;
}
