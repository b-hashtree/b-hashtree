/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
                      Daniel García-García  (UPM)
 *                    Pablo García-Hernández (UPM)
 *
 * This file is part of B-hashtree Library.
 *
 *     B-hashtree Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     B-hashtree Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with B-hashtree Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

import java.util.*;

public class Benchmark {

    final static Random random = new Random(30);

    public static ArrayList<Double> evaluateString(Map<Vector3, AnglePass> map, int size, int numberOfOp) {
        ArrayList<Double> ellapsed = new ArrayList<>();

        ellapsed.add(System.nanoTime() / 1e6); // Take time in milliSeconds

        /*
         * ANGLE_INC_DEGREES = 360 / SECTORS
         * alpha = (i % SECTORS) * ANGLE_INC_DEGREES * PI / 180
         * x = RADIUS * sin(alpha)
         * y = RADIUS * cos(alpha)
         * z = SLOPE * i
         */

        AnglePass[] anglePasses = new AnglePass[size];
        Vector3[] points = new Vector3[size];

        for (int i = 0; i < size; ++i) {
            AnglePass anglePass = new AnglePass(i);
            Vector3 key = AnglePass.calculate(anglePass);
            anglePasses[i] = anglePass;
            points[i] = key;
            map.put(key, anglePass);
        }

        ellapsed.add(System.nanoTime() / 1e6); // Take time in milliSeconds

        for (int j = 0; j < numberOfOp; ++j) {
            int index = random.nextInt(size);
            Vector3 key = points[index];
            AnglePass mapped = map.get(key);
            mapped.accessed += 1;
            Vector3 checking = AnglePass.calculate(mapped);
            if (!key.match(checking)) {
                System.err.println("Error when ckecking Vector3. Should not be possible to get here");
                System.exit(2);
            }
        }

        ellapsed.add(System.nanoTime() / 1e6); // Take time in milliSeconds

        int totalAccessed = 0;
        for (AnglePass anglePass : anglePasses) {
            totalAccessed += anglePass.accessed; 
        }

        if (totalAccessed != numberOfOp) {
            System.err.println("Error when ckecking number of operations. Should not be possible to get here");
            System.exit(3);
        }

        return ellapsed;
    }

    public static void main(String[] args) {
        int logSize = Integer.parseInt(args[1]);
        int size = 1 << logSize; // 2 ** logSize
        int numberOfOp = Integer.parseInt(args[2]);
        ArrayList<Double> ellapsed = null;

        Map<Vector3, AnglePass> map;
        if (args[0].equalsIgnoreCase("Hashtable")) {
            map = new Hashtable<>();
            ellapsed = evaluateString(map, size, numberOfOp);
        } else if (args[0].equalsIgnoreCase("HashMap")) {
            map = new HashMap<>();
            ellapsed = evaluateString(map, size, numberOfOp);
        } else if (args[0].equalsIgnoreCase("CuckooTable")) {
            map = new bhashtree.CuckooTable<>();
            ellapsed = evaluateString(map, size, numberOfOp);
        }

        for (int i = 1; i < ellapsed.size(); ++i) {
            System.out.print(String.format(Locale.US, "%.3f\t", ellapsed.get(i) - ellapsed.get(i - 1)));
        }
        int[] numberofIterations = { size, numberOfOp };
        for (int i = 1; i < ellapsed.size(); ++i) {
            double speed = numberofIterations[i-1] / (ellapsed.get(i) - ellapsed.get(i - 1)); // number of iterations per **milli** second
            System.out.print(String.format(Locale.US,"%.3f\t", speed));
        }
        System.out.println();
    }

}

class Vector3 {
    final static double PRECISION = 1E-10;
    public double x, y, z;

    public boolean match(Vector3 v) {
        return Math.abs(x - v.x) < PRECISION && Math.abs(y - v.y) < PRECISION && Math.abs(z - v.z) < PRECISION;
    }
}

class AnglePass {
    static final int SECTORS = 24;
    static final int ANGLE_INC_DEGREES = 360 / SECTORS;
    static final double RADIUS = 1.1;
    static final double SLOPE = 0.017;

    public double alpha;
    public int index;
    public int accessed = 0;

    public AnglePass(int i) {
        index = i;
        alpha = (i % SECTORS) * ANGLE_INC_DEGREES * Math.PI / 180;
    }

    public static Vector3 calculate(AnglePass anglePass) {
        Vector3 v3 = new Vector3();
        v3.x = RADIUS * Math.sin(anglePass.alpha);
        v3.y = RADIUS * Math.cos(anglePass.alpha);
        v3.z = SLOPE * anglePass.index;
        return v3;
    }
}