""" ***************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
                      Daniel García-García  (UPM)
 *                    Pablo García-Hernández (UPM)
 *
 * This file is part of B-hashtree Library.
 *
 *     B-hashtree Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     B-hashtree Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with B-hashtree Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/ """

import time
import sys
import math
import random
import os 

if __name__ == "__main__":
    dir_path = os.path.dirname(os.path.realpath(__file__))
    if os.getcwd() == dir_path:
        sys.path.append("../..")
    else:
        print("Error: this script is designed to be executed in his own folder as working dir")
        sys.exit(22)

from bhashtree.cuckoo_table import CuckooTable

class Vector3: 
    PRECISION = 1E-10
    def __init__(self) -> None:
        self.x = 0
        self.y = 0
        self.z = 0

    def __eq__(self, v: object) -> bool:
        return abs(self.x - v.x) < Vector3.PRECISION and abs(self.y - v.y) < Vector3.PRECISION and abs(self.z - v.z) < Vector3.PRECISION

    def __hash__(self):
        a_tuple = (self.x, self.y, self.z)
        return hash(a_tuple)        

class AnglePass:
    SECTORS = 24
    ANGLE_INC_DEGREES = 360 / SECTORS
    RADIUS = 1.1
    SLOPE = 0.017

    def __init__(self, i: int) -> None:
        self.index = i
        self.alpha = (i % AnglePass.SECTORS) * AnglePass.ANGLE_INC_DEGREES * math.pi / 180
        self.accessed = 0

    @staticmethod
    def calculate(anglePass: object) -> object:
        v3 = Vector3()
        v3.x = AnglePass.RADIUS * math.sin(anglePass.alpha)
        v3.y = AnglePass.RADIUS * math.cos(anglePass.alpha)
        v3.z = AnglePass.SLOPE * anglePass.index
        return v3

class CaseE:

    @staticmethod
    def evaluate(map, size, numberOfOp):
        ellapsed = [ time.time_ns()/1e6 ] # milliseconds
        
        """
        /* ANGLE_INC_DEGREES = 360 / SECTORS
         * alpha = (i % SECTORS) * ANGLE_INC_DEGREES * PI / 180
         * x = RADIUS * sin(alpha)
         * y = RADIUS * cos(alpha)
         * z = SLOPE * i
         */"""

        anglePasses = []
        points = []

        for i in range(0,size):
            anglePass = AnglePass(i)
            key = AnglePass.calculate(anglePass)
            anglePasses.append(anglePass)
            points.append(key)
            map[key] = anglePass

        ellapsed.append(time.time_ns()/1e6)

        for j in range(0, numberOfOp):
            index = random.randrange(0,size-1)
            key = points[index]
            mapped = map[key]
            mapped.accessed += 1
            checking = AnglePass.calculate(mapped)
            if not key == checking:
                sys.stderr.write("Error when ckecking Vector3. Should not be possible to get here\n")
                sys.exit(2)
    
        ellapsed.append(time.time_ns()/1e6)

        totalAccessed = 0
        for anglePass in anglePasses:
            totalAccessed += anglePass.accessed 

        if totalAccessed != numberOfOp:
            sys.stderr.write("Error when ckecking number of operations. Should not be possible to get here\n")
            sys.exit(3)

        return ellapsed

    @staticmethod
    def main():
        logSize = int(sys.argv[1])
        size = 1 << logSize
        numberOfOp = int(sys.argv[2])
        which = str(sys.argv[3])
        if which == "dict": map = {}
        elif which == "cuckoo-table": map = CuckooTable()
        else:
            sys.stderr.write("Unknown class: " + which + "\n")
            sys.exit(22)

        ellapsed = CaseE.evaluate(map,size,numberOfOp)

        for i in range(1,len(ellapsed)):
            print(ellapsed[i]-ellapsed[i-1], end='\t')
        
        numberofIterations = [ size, numberOfOp] 
        for i in range(1,len(ellapsed)):
            speed = numberofIterations[i-1] / (ellapsed[i]-ellapsed[i-1])
            print(speed, end='\t')
        print()

if __name__ == "__main__":
    CaseE.main()
