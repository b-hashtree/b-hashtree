/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
                      Daniel García-García  (UPM)
 *                    Pablo García-Hernández (UPM)
 *
 * This file is part of B-hashtree Library.
 *
 *     B-hashtree Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     B-hashtree Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with B-hashtree Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include <string>
#include <iostream>
#include <sstream>
#include <random>
#include <algorithm>
#include <iterator>
#include <chrono>
#include <cstdio>
#include <unordered_map>
#include <map>

#ifdef DEBUGGING_OUTPUTS
#define LOG_LEVEL 15
#else
#define LOG_LEVEL 0
#endif

#include "../shuffle_array.hpp"

using namespace std;

typedef pair<chrono::time_point<chrono::high_resolution_clock>, string> checkpoint_t;
typedef vector<checkpoint_t> checkpoints_t;

template <class table_t>
int case_a(size_t size, unsigned shift, checkpoints_t &ellapsed)
{
  ellapsed.push_back(checkpoint_t(chrono::high_resolution_clock::now(), "Start")); // Taking time

  vector<int32_t> numbers(size, -1);

  table_t table;

  unsigned seed = 30;
  default_random_engine random_eng(seed);
  uniform_real_distribution<> rrand(0, 1);

#if LOG_LEVEL > 1
  cerr << "Array Size: " << size << endl;
#endif

  // Fill vector from 1 to a_size
  int32_t counter = 0;
  generate_n(numbers.begin(), size, [&counter, shift]() -> int32_t
             { return shift * counter++; });

  // Shuffle
  shuffle_array(numbers);

  ellapsed.push_back(checkpoint_t(chrono::high_resolution_clock::now(), "Random Vector")); // Taking time

#if LOG_LEVEL > 10
  cerr << "Original random vector" << endl;
  copy(numbers.begin(), numbers.end(), ostream_iterator<int>(cerr, " "));
  cerr << endl;
#endif

  // Build Map (no duplicates in this benchmark)
  for (unsigned i = 0; i < size; ++i)
  {
    typename table_t::key_type key = numbers[i];
    double number = rrand(random_eng);
#if LOG_LEVEL > 12
    cerr << key << " -> " << number << endl;
#endif
    table.insert(typename table_t::value_type(key, number));
  }

  ellapsed.push_back(checkpoint_t(chrono::high_resolution_clock::now(), "Build Map")); // Taking time

  // Get values and sum up
  double sum = 0;
  for (size_t i = 0; i < size; ++i)
  {
    auto key = numbers[i];
    double number = table.at(key);
    sum += number;
#if LOG_LEVEL > 12
    cerr << key << " -> " << number << " sum: " << sum << endl;
#endif
  }
  cerr << "Average: " << (sum / size) << endl;

  ellapsed.push_back(checkpoint_t(chrono::high_resolution_clock::now(), "Lookups")); // Taking time

  for (size_t i = 0; i < size; ++i)
  {
    auto key = numbers[i];
    table.erase(key);
  }

  ellapsed.push_back(checkpoint_t(chrono::high_resolution_clock::now(), "Removals")); // Taking time
  return 0;
}

int main(int argc, char **argv)
{
  int exponent = 5;
  unsigned shift = 0;

  if (argc >= 2)
  {
    istringstream arg1(argv[1]);
    arg1 >> exponent;
  }

  if (argc >= 3)
  {
    istringstream arg2(argv[2]);
    arg2 >> shift;
  }

  int returning = 0;
  checkpoints_t time_ellapseds;
  size_t size = 1 << exponent; // 2 ** exponent

  returning = case_a<unordered_map<int32_t, double>>(size, shift, time_ellapseds);

  for (size_t i = 1; i < time_ellapseds.size(); ++i)
  {
    auto t = chrono::duration_cast<chrono::duration<double>>(time_ellapseds[i].first - time_ellapseds[i - 1].first); // seconds
    printf("%.3f\t", size / 1e3 / t.count());
  }
  printf("\n");
  return returning;
}
