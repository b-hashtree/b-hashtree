""" ***************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
                      Daniel García-García  (UPM)
 *                    Pablo García-Hernández (UPM)
 *
 * This file is part of B-hashtree Library.
 *
 *     B-hashtree Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     B-hashtree Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with B-hashtree Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/ """

import time
import sys
import random
import os 

if __name__ == "__main__":
    dir_path = os.path.dirname(os.path.realpath(__file__))
    if os.getcwd() == dir_path:
        sys.path.append("../..")
    else:
        print("Error: this script is designed to be executed in his own folder as working dir")
        sys.exit(22)

from bhashtree.cuckoo_table import CuckooTable

class CaseA:

    @staticmethod
    def shuffle(array):
        random.seed(33)
        i = len(array) - 1
        while i > 0 :
            index = random.randint(0, i-1)
            array[index], array[i] = array[i], array[index]
            i -= 1

    @staticmethod
    def evaluateInt(map, size, shift):
        ellapsed = []
        sum = 0
        ellapsed.append(time.time_ns()/1e6)

        numbers=[]
        for i in range(0,size):
            numbers.append(i*shift)
    
        CaseA.shuffle(numbers)

        ellapsed.append(time.time_ns()/1e6)

        for i in range(0,size):
            key = numbers[i]
            map[key] = random.uniform(0,1)
    
        ellapsed.append(time.time_ns()/1e6)

        for i in range(0,size):
            key = numbers[i]
            sum += map[key]
    
        sys.stderr.write("Average is " + str(sum / size) + "\n")
        
        ellapsed.append(time.time_ns()/1e6)
        
        for i in range(0,size):
            key = numbers[i]
            map.pop(key)          
        
        ellapsed.append(time.time_ns()/1e6)
        
        return ellapsed
        
    @staticmethod
    def main():
        logSize = int(sys.argv[1])
        size = 1 << logSize
        shift = int(sys.argv[2])
        which = str(sys.argv[3])
        if which == "dict": map = {}
        elif which == "cuckoo-table": map = CuckooTable()
        else:
            sys.stderr.write("Unknown class: " + which + "\n")
            sys.exit(22)

        ellapsed = CaseA.evaluateInt(map,size,shift)

        for x in range(1,len(ellapsed)):
            speed = size / (ellapsed[x] - ellapsed[x-1]) 
            print(speed, end="\t")

        print()

if __name__ == "__main__":
    CaseA.main()
