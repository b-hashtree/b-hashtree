#!/usr/bin/env php

<?php

/* ***************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
                      Daniel García-García  (UPM)
 *                    Pablo García-Hernández (UPM)
 *
 * This file is part of B-hashtree Library.
 *
 *     B-hashtree Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     B-hashtree Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with B-hashtree Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

function evaluateInt($map, $logSize, $shift){
    $ellapsed = [];
    $suma=0;

    $ellapsed[]=microtime(true)*1000;
    $size = 1 << $logSize;
    $numbers=[];
    for($i=0;$i<$size;$i++){
        $numbers[]=$i*$shift;
    }
    shuffle($numbers);

    $ellapsed[]=microtime(true)*1000;

    for($i=0;$i<$size;$i++){
        $key=$numbers[$i];
        $map[$key]=1/rand(1,9);
    }

    $ellapsed[]=microtime(true)*1000;

    for($i=0;$i<$size;$i++){
        $key=$numbers[$i];
        $suma+=$map[$key];
    }

    $ellapsed[]=microtime(true)*1000;

    for($i=0;$i<$size;$i++){
        $key=$numbers[$i];
        unset($map[$key]);
    }

    $ellapsed[]=microtime(true)*1000;

    $ellapsed[]=$suma;



    return $ellapsed;
}

$map = [];
$logSize = $argv[1];
$shift = $argv[2];


$ellapsed = evaluateInt($map,$logSize,$shift);
print $logSize;
print "\t";
print $shift;
print "\t";
for($i=1;$i < count($ellapsed);$i++){
    $a= $ellapsed[$i];
    $b= $ellapsed[$i-1];
    print($a-$b);
    print "\t";
}
?>