
# CASO A

* Queremos estudiar el tiempo de construcción solo parametrizamos
con shift. Nada más.

* Mapea números consecutivos entre 0 y Size-1 barajados para 
introducirlo de forma aleatoria con un número real, alternando
uno positivo y uno negativo.

* Hacemos la suma de 128 números y salimos.

* Nos interesa tiempo y memoria totales. No vamos a mirar parciales
de tiempos. 