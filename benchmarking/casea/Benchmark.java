/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
                      Daniel García-García  (UPM)
 *                    Pablo García-Hernández (UPM)
 *
 * This file is part of B-hashtree Library.
 *
 *     B-hashtree Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     B-hashtree Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with B-hashtree Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

import java.util.*;

public class Benchmark {

    public static ArrayList<Double> evaluateInt(Map<Integer, Double> map, int size, int shift) {
        ArrayList<Double> ellapsed = new ArrayList<>();

        ellapsed.add(System.nanoTime() / 1e6); // Take time in milliSeconds

        // Build Array
        int[] numbers = new int[size];
        for (int i = 0; i < size; ++i) {
            // array of numbers apart by shift
            numbers[i] = i * shift;
        }

        shuffleArray(numbers);

        ellapsed.add(System.nanoTime() / 1e6); // Take time in milliSeconds

        // Build Map
        for (int i = 0; i < size; ++i) {
            int key = numbers[i];
            map.put(key, Math.random());
        }

        ellapsed.add(System.nanoTime() / 1e6); // Take time in milliSeconds

        // Get values
        double sum = 0;
        for (int i = 0; i < size; ++i) {
            int key = numbers[i];
            sum += map.get(key);
        }
        System.err.println("Average: " + (sum/size));

        ellapsed.add(System.nanoTime() / 1e6); // Take time in milliSeconds

        for (int i = 0; i < size; ++i) {
            int key = numbers[i];
            map.remove(key);
        }

        ellapsed.add(System.nanoTime() / 1e6); // Take time in milliSeconds

        return ellapsed;
    }

    static void shuffleArray(int[] ar) {

        Random rnd = new Random();
        for (int i = ar.length - 1; i > 0; i--) {
            Integer index = rnd.nextInt(i + 1);
            // Simple swap
            int a = ar[index];
            ar[index] = ar[i];
            ar[i] = a;
        }
    }

    public static void main(String[] args) {
        int logSize = Integer.parseInt(args[0]);
        int size = 1 << logSize; // 2 ** logSize

        int shift = Integer.parseInt(args[1]);
        ArrayList<Double> ellapsed = null;

        Map<Integer, Double> map;
        map = new HashMap<>();
        ellapsed = evaluateInt(map, size, shift);

        for (int i = 1; i < ellapsed.size(); ++i) {
            double res = size / (ellapsed.get(i) - ellapsed.get(i - 1));
            System.out.print(res + "\t");
        }
        System.out.println();
    }

}
