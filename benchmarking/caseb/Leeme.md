
# Caso B

* Mapea {números consecutivos entre 0 y Size-1 a los que se 
aplicado un desplazamiento a izquierdas y que se almacenan en
un vector y se barajan } al { índice+1 } que ocupan en dicho vector.

* Se hace un número de operaciones de búsqueda fijo siguiendo
el orden en el vector y se van sumando los valores.

* Cada vez que se llega a Size se comprueba que la suma es
Size(Size+1)/2. 

* __Atención__ debido a que en Java el hashCode devuelve un int
en el resto de lenguaje tenemos que utilizar también int
y entonces el shift desplazamiento máximo debe ser 10 para
que quepan los números hasta 2^22.