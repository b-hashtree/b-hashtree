#! /bin/bash

# /* ***************************************************************************
# * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
#                      Daniel García-García (UPM)
# *                    Pablo García-Hernández (UPM)
# *
# * This file is part of B-hashtree Library.
# *
# *     B-hashtree Library is free software: you can redistribute it and/or modify
# *     it under the terms of the GNU General Public License as published by
# *     the Free Software Foundation, either version 3 of the License, or
# *     (at your option) any later version.
# *
# *     B-hashtree Library is distributed in the hope that it will be useful,
# *     but WITHOUT ANY WARRANTY; without even the implied warranty of
# *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# *     GNU General Public License for more details.
# *
# *     You should have received a copy of the GNU General Public License
# *     along with B-hashtree Library.  If not, see <http://www.gnu.org/licenses/>.
# *****************************************************************************/

INTERNAL_TIME="cpp-output.tsv"
LOGGING_FILE="cpp-error.log"
EXTERNAL_TIME="cpp-time.tsv"

CASE_EXEC=$1

# Check CASE. Case data header might case from case to case (program steps)
printf "Class\tShift\tLogSize\tSize\tRandomVector\tBuildMap\tLookups\n" > $INTERNAL_TIME

printf "Class\tShift\tLogSize\tSize\tTime\tMemory\tSystemTime\tUserTime\n" > $EXTERNAL_TIME
printf "Logging\n" > $LOGGING_FILE

# Measure time and memory with linux command
MEASURE_CMD="/bin/time -o $EXTERNAL_TIME --append -f %e\t%M\t%S\t%U"

# Check CASE. NUMBER_OF_OP might be changed for case to case or ignored
# Number of operations is double the maximum size
NUMBER_OF_OP=$((2 ** 23))

# Check CASE. These loops depends on the case parameters
for CL in a m u c h; do
  for LogSize in 10 11 12 13 14 15 16 17 18 19 20 21; do
    for shift in 0 5 10; do
      SIZE=$((2 ** $LogSize))
      # Check CASE. This variable depends on which are the case parameters. 
      THE_COMMAND="$CASE_EXEC $LogSize $CL $shift $NUMBER_OF_OP"
      echo "[run_cases.sh] running: $THE_COMMAND >> $INTERNAL_TIME 2>> $LOGGING_FILE"
      printf "$CL\t$shift\t$LogSize\t$SIZE\t" >> $EXTERNAL_TIME
      printf "$CL\t$shift\t$LogSize\t$SIZE\t" >> $INTERNAL_TIME
      $MEASURE_CMD $THE_COMMAND >> $INTERNAL_TIME 2>> $LOGGING_FILE || exit 55
    done
  done
done
