""" ***************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
                      Daniel García-García  (UPM)
 *                    Pablo García-Hernández (UPM)
 *
 * This file is part of B-hashtree Library.
 *
 *     B-hashtree Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     B-hashtree Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with B-hashtree Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/ """

import time
import sys
import random
import os 

if __name__ == "__main__":
    dir_path = os.path.dirname(os.path.realpath(__file__))
    if os.getcwd() == dir_path:
        sys.path.append("../..")
    else:
        print("Error: this script is designed to be executed in his own folder as working dir")
        sys.exit(22)

from bhashtree.cuckoo_table import CuckooTable

class CaseB:

    @staticmethod
    def shuffle(array):
        random.seed(33)
        i = len(array) - 1
        while i > 0 :
            index = random.randint(0, i-1)
            array[index], array[i] = array[i], array[index]
            i -= 1


    @staticmethod
    def evaluate(map, size, shift, numberOfOp):
        ellapsed = [ time.time_ns()/1e6 ] # milliseconds

        numbers = [ i << shift for i in range(0,size) ]
    
        CaseB.shuffle(numbers)

        ellapsed.append(time.time_ns()/1e6)
        
        # Build Map (there are not any duplicates)
        for i, key in enumerate(numbers):
            map[key] = i + 1
    
        ellapsed.append(time.time_ns()/1e6)

        n = 0
        sum = 0

        for i in range(0,numberOfOp):
            key = numbers[i % size]
            number = map[key]
            sum += number
            n += 1
            if n == size: 
                # Sum from 1..n = n*(n+1) / 2
                expected = (n * (n + 1) / 2)
                if sum != expected : 
                    sys.stderr.write("Compute sum is " + str(sum) + " expected sum is " + str(expected))
                    sys.exit(2)
                
                n = 0
                sum = 0
    
        ellapsed.append(time.time_ns()/1e6)

        return ellapsed

    @staticmethod
    def main():
        logSize = int(sys.argv[1])
        size = 1 << logSize
        shift = int(sys.argv[2])
        numberOfOp = int(sys.argv[3])
        which = str(sys.argv[4])
        if which == "dict": map = {}
        elif which == "cuckoo-table": map = CuckooTable()
        else:
            sys.stderr.write("Unknown class: " + which + "\n")
            sys.exit(22)

        ellapsed = CaseB.evaluate(map,size,shift, numberOfOp)

        numberofIterations = [size, size, numberOfOp] 
        for i in range(1,len(ellapsed)):
            speed = numberofIterations[i-1] / (ellapsed[i]-ellapsed[i-1])
            print(speed, end='\t')
        print()

if __name__ == "__main__":
    CaseB.main()
