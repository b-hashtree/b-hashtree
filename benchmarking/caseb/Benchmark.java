/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
                      Daniel García-García  (UPM)
 *                    Pablo García-Hernández (UPM)
 *
 * This file is part of B-hashtree Library.
 *
 *     B-hashtree Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     B-hashtree Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with B-hashtree Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

import java.util.*;

public class Benchmark {

    final static Random random = new Random(121);

    public static ArrayList<Double> evaluateInt(Map<Integer, Integer> map, int size, int shift, int numberOfOp) {
        ArrayList<Double> ellapsed = new ArrayList<>();

        ellapsed.add(System.nanoTime() / 1e6); // Take time in milliSeconds

        int[] numbers = new int[size];
        for (int i = 0; i < size; ++i) {
            numbers[i] = i << shift;
        }

        benchmarking.ShuffleArray.shuffle(numbers);

        ellapsed.add(System.nanoTime() / 1e6); // Take time in milliSeconds

        // Build Map (there are not any duplicates)
        for (int i = 0; i < size; ++i) {
            Integer key = numbers[i];
            map.put(key, i + 1);
        }

        ellapsed.add(System.nanoTime() / 1e6); // Take time in milliSeconds

        // Get values and sum up
        long n = 0;
        long sum = 0;

        for (int i = 0; i < numberOfOp; ++i) {
            Integer key = numbers[i % size];
            Integer number = map.get(key);
            sum += number;
            ++n;
            if (n == size) {
                // Sum from 1..n = n*(n+1) / 2
                long expected = n / 2 * (n + 1);
                if (sum != expected) {
                    System.err.println("Compute sum is " + sum + " expected sum is " + expected);
                }
                n = 0;
                sum = 0;
            }
        }

        ellapsed.add(System.nanoTime() / 1e6); // Take time in milliSeconds
        return ellapsed;
    }

    public static void main(String[] args) {
        int logSize = Integer.parseInt(args[1]);
        int size = 1 << logSize; // 2 ** logSize
        int shift = Integer.parseInt(args[2]);
        int numberOfOp = Integer.parseInt(args[3]);
        ArrayList<Double> ellapsed = null;

        Map<Integer, Integer> map;
        if (args[0].equalsIgnoreCase("Hashtable")) {
            map = new Hashtable<>();
            ellapsed = evaluateInt(map, size, shift, numberOfOp);
        } else if (args[0].equalsIgnoreCase("HashMap")) {
            map = new HashMap<>();
            ellapsed = evaluateInt(map, size, shift, numberOfOp);
        } else if (args[0].equalsIgnoreCase("CuckooTable")) {
            map = new bhashtree.CuckooTable<>();
            ellapsed = evaluateInt(map, size, shift, numberOfOp);
        }

        int[] numberofIterations = { size, size, numberOfOp };
        for (int i = 1; i < ellapsed.size(); ++i) {
            double speed = numberofIterations[i-1] / (ellapsed.get(i) - ellapsed.get(i - 1)); // number of iterations per **milli** second
            System.out.print(String.format(Locale.US,"%.3f\t", speed));
        }
        System.out.println();
    }

}
