/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
                      Daniel García-García  (UPM)
 *                    Pablo García-Hernández (UPM)
 *
 * This file is part of B-hashtree Library.
 *
 *     B-hashtree Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     B-hashtree Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with B-hashtree Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include <string>
#include <iostream>
#include <sstream>
#include <random>
#include <algorithm>
#include <iterator>
#include <chrono>
#include <cstdio>
#include <unordered_map>
#include <map>

#ifdef DEBUGGING_OUTPUTS
#define LOG_LEVEL 15
#else
#define LOG_LEVEL 0
#endif

#include "../shuffle_array.hpp"

#include "bhashtree/array_adapter.hpp"
#include "bhashtree/cuckoo_table.hpp"
#include "bhashtree/hamt.hpp"

using namespace std;

typedef pair<chrono::time_point<chrono::high_resolution_clock>, string> checkpoint_t;
typedef vector<checkpoint_t> checkpoints_t;

template <class table_t, typename convert2key>
int case_b(size_t size, unsigned shift, size_t numberOfOp, checkpoints_t &ellapsed)
{
  ellapsed.push_back(checkpoint_t(chrono::high_resolution_clock::now(), "Start")); // Taking time

  vector<int32_t> numbers(size, -1);

  table_t table;

#if LOG_LEVEL > 1
  cerr << "Array Size: " << size << endl;
#endif

  // Fill vector from 1 to size
  int32_t counter = 0;
  generate_n(numbers.begin(), size, [&counter, shift]() -> int32_t
             { return counter++ << shift; });

  // Shuffle
  shuffle_array(numbers);

  ellapsed.push_back(checkpoint_t(chrono::high_resolution_clock::now(), "Random Vector")); // Taking time

#if LOG_LEVEL > 10
  cerr << "Original random vector" << endl;
  copy(numbers.begin(), numbers.end(), ostream_iterator<int>(cerr, " "));
  cerr << endl;
#endif

  // Build Map (no duplicates in this benchmark)
  for (unsigned i = 0; i < size; ++i)
  {
    typename table_t::key_type key = convert2key(numbers[i]);
    table.insert(typename table_t::value_type(key, i + 1)); // Value in map from 1 to size
  }

  ellapsed.push_back(checkpoint_t(chrono::high_resolution_clock::now(), "Build Map")); // Taking time

  // Check put and get (when the key exists)
  size_t n = 0; 
  uint64_t sum = 0;
  for (size_t i = 0; i < numberOfOp; ++i)
  {
    typename table_t::key_type key = convert2key(numbers[i % size]);
    unsigned number = table.at(key);
    sum += number;
    ++n;
    if (n == size)
    {
      if (sum != n * (n + 1) / 2)
      {
        // Sum from 1..n = n*(n+1) / 2
        cout << "Compute sum is " << sum << " expected sum is " << n * (n + 1) / 2 << endl;
        exit(2);
      }
      n = 0;
      sum = 0;
    }
  }
  ellapsed.push_back(checkpoint_t(chrono::high_resolution_clock::now(), "Lookups")); // Taking time
  return 0;
}

int main(int argc, char **argv)
{
  int exponent = 5;
  char which = 'c';
  unsigned shift = 0;
  size_t numberOfOp = 100;

  if (argc >= 2)
  {
    istringstream arg1(argv[1]);
    arg1 >> exponent;
  }

  if (argc >= 3)
  {
    istringstream arg2(argv[2]);
    arg2 >> which;
  }

  if (argc >= 4)
  {
    istringstream arg2(argv[3]);
    arg2 >> shift;
  }

  if (argc >= 5)
  {
    istringstream arg2(argv[4]);
    arg2 >> numberOfOp;
  }

  int returning = 0;
  checkpoints_t time_ellapseds;
  size_t size = 1 << exponent; // 2 ** exponent

  switch (which)
  {
  case 'a':
    array_adapter<int32_t, unsigned>::shift = shift;
    returning = case_b<array_adapter<int32_t, unsigned>, unsigned>(size, shift, numberOfOp, time_ellapseds);
    break;
  case 'm':
    returning = case_b<map<int32_t, unsigned>, unsigned>(size, shift, numberOfOp, time_ellapseds);
    break;
  case 'u':
    returning = case_b<unordered_map<int32_t, unsigned>, unsigned>(size, shift, numberOfOp, time_ellapseds);
    break;
  case 'h':
    returning = case_b<hamt<int32_t, unsigned>, unsigned>(size, shift, numberOfOp, time_ellapseds);
    break;
  default:
    returning = case_b<cuckoo_table<int32_t, unsigned>, unsigned>(size, shift, numberOfOp, time_ellapseds);
    break;
  }
  size_t numberofIterations[] = { size, size, numberOfOp };
  for (size_t i = 1; i < time_ellapseds.size(); ++i)
  {
    auto t = chrono::duration_cast<chrono::duration<double>>(time_ellapseds[i].first - time_ellapseds[i - 1].first); // seconds
    double speed = numberofIterations[i-1] / t.count() / 1e3; // number of iterations per **milli** second
    printf("%.3f\t", speed);
  }
  printf("\n");
  return returning;
}
