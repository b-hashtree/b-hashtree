/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
                      Daniel García-García  (UPM)
 *                    Pablo García-Hernández (UPM)
 *
 * This file is part of B-hashtree Library.
 *
 *     B-hashtree Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     B-hashtree Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with B-hashtree Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

package benchmarking;

import java.util.Random;

public class ShuffleArray {

    final static int RAND_SEED = 33;

    public static void shuffle(int[] array)
    {
        Random rnd = new Random(RAND_SEED);
        for (int i = array.length - 1; i > 1; i--)
        {
            int index = rnd.nextInt(i);
            int aux = array[i];
            array[i] = array[index];
            array[index] = aux;
        }
    }
}