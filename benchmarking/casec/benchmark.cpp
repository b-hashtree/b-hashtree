/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
                      Daniel García-García  (UPM)
 *                    Pablo García-Hernández (UPM)
 *
 * This file is part of B-hashtree Library.
 *
 *     B-hashtree Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     B-hashtree Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with B-hashtree Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include <string>
#include <iostream>
#include <sstream>
#include <random>
#include <algorithm>
#include <iterator>
#include <chrono>
#include <cstdio>
#include <unordered_map>
#include <map>
#include <tuple>
#include <deque>

#ifdef DEBUGGING_OUTPUTS
#define LOG_LEVEL 15
#else
#define LOG_LEVEL 0
#endif

#include "bhashtree/cuckoo_table.hpp"
#include "bhashtree/hamt.hpp"

class ItemValue
{
public:
  ItemValue(int k) : key(k)
  {
  }

  /*
     * Si está o no insertado.
     * Las veces que se ha eliminado.
     * Las veces que se ha accedido a él.
     * Las veces que se ha insertado (después de la primera)
     */
  int key;
  bool isInserted = true;
  int numberOfRemovals = 0;
  int numberOfAccess = 0;
  int numberOfNotFound = 0;
  int numberOfInsertions = 0;
};

using namespace std;

typedef pair<chrono::time_point<chrono::high_resolution_clock>, string> checkpoint_t;
typedef vector<checkpoint_t> checkpoints_t;

template <class table_t>
void case_c(size_t size, double p, size_t numberOfOp, checkpoints_t &ellapsed)
{
  vector<ItemValue> values;
  values.reserve(size);
  table_t map;
  unsigned seed = 30;
  default_random_engine random_eng(seed);
  uniform_int_distribution<unsigned> uniform_dist(0, 32 * size);
  uniform_real_distribution<> rrand(0, 1);

  ellapsed.push_back(checkpoint_t(chrono::high_resolution_clock::now(), "Start")); // Taking time

  // Build Map (checking duplicate in this benchmark)
  unsigned i = 0;
  while (i < size)
  {
    typename table_t::key_type key = uniform_dist(random_eng);
    if (map.count(key) == 0)
    {
      ItemValue item(key);
      map.insert(typename table_t::value_type(key, item));
      values.push_back(item);
      ++i;
    }
  }

  ellapsed.push_back(checkpoint_t(chrono::high_resolution_clock::now(), "Build Map")); // Taking time
  // Get, Remove or Insert values
  deque<ItemValue *> deque;
  int dequeSizeLimit = 10;

  for (int i = 0; i < numberOfOp; ++i)
  {
    double chance = rrand(random_eng);
    if (chance < p)
    { // Access
      ItemValue &aValue = values[i % size];
      try
      {
        ItemValue &iValue = map.at(aValue.key);
        iValue.numberOfAccess += 1;
        aValue.numberOfAccess += 1;
        if (aValue.key != iValue.key)
        {
          cerr << "Keys should be equals. Should not be possible to get here" << endl;
          exit(20);
        }
        if (!iValue.isInserted)
        {
          cerr << "Item should be inserted. Should not be possible to get here" << endl;
          exit(21);
        }
      }
      catch (...)
      {
        aValue.numberOfNotFound += 1;
      }
    }
    else if (deque.size() <= dequeSizeLimit && (deque.size() == 0 || chance < p + (1 - p) / 2))
    { // Remove
      int j = i;
      while (!values[j % size].isInserted)
      {
        j += 1;
      }
      ItemValue &aValue = values[j % size];
      map.erase(aValue.key);
      aValue.isInserted = false;
      aValue.numberOfRemovals += 1;
      deque.push_back(&aValue);
    }
    else
    { // Insert
      ItemValue *iValue = deque.front();
      deque.pop_front();
      iValue->numberOfInsertions += 1;
      iValue->isInserted = true;
      map.insert(typename table_t::value_type(iValue->key, *iValue));
    }
  }

  ellapsed.push_back(checkpoint_t(chrono::high_resolution_clock::now(), "Lookups")); // Taking time

  int totalAccess = 0;
  int totalInsertions = 0;
  int totalRemovals = 0;

  // Checking operations
  for (const ItemValue &aValue : values)
  {
    numberOfOp -= aValue.numberOfAccess;
    numberOfOp -= aValue.numberOfInsertions;
    numberOfOp -= aValue.numberOfNotFound;
    numberOfOp -= aValue.numberOfRemovals;
    totalAccess += aValue.numberOfAccess + aValue.numberOfNotFound;
    totalInsertions += aValue.numberOfInsertions;
    totalRemovals += aValue.numberOfRemovals;
  }

  fprintf(stderr, "Access: %d, Insertions: %d, Removals: %d\n", totalAccess, totalInsertions, totalRemovals);

  if (numberOfOp != 0)
  {
    fprintf(stderr, "NumberOfOp is not compute correctly. Should not be possible to get here\n");
    exit(22);
  }

  //ellapsed.push_back(checkpoint_t(chrono::high_resolution_clock::now(), "Checking")); // Taking time
}

int main(int argc, char **argv)
{
  int exponent = 5;
  char which = 'c';
  double p = 0.5;
  size_t numberOfOp = 100;

  if (argc >= 2)
  {
    istringstream arg1(argv[1]);
    arg1 >> exponent;
  }

  if (argc >= 3)
  {
    istringstream arg2(argv[2]);
    arg2 >> which;
  }

  if (argc >= 4)
  {
    istringstream arg2(argv[3]);
    arg2 >> p;
  }

  if (argc >= 5)
  {
    istringstream arg2(argv[4]);
    arg2 >> numberOfOp;
  }

  checkpoints_t time_ellapseds;
  size_t size = 1 << exponent; // 2 ** exponent

  switch (which)
  {
  case 'm':
    case_c<map<int32_t, ItemValue>>(size, p, numberOfOp, time_ellapseds);
    break;
  case 'u':
    case_c<unordered_map<int32_t, ItemValue>>(size, p, numberOfOp, time_ellapseds);
    break;
  case 'h':
    case_c<hamt<int32_t, ItemValue>>(size, p, numberOfOp, time_ellapseds);
    break;
  case 'c':
    case_c<cuckoo_table<int32_t, ItemValue>>(size, p, numberOfOp, time_ellapseds);
    break;
  default:
    exit(99);
    break;
  }

  size_t numberofIterations[] = { size, numberOfOp };
  for (size_t i = 1; i < time_ellapseds.size(); ++i)
  {
    auto t = chrono::duration_cast<chrono::duration<double>>(time_ellapseds[i].first - time_ellapseds[i - 1].first); // seconds
    double speed = numberofIterations[i-1] / t.count() / 1e3; // number of iterations per **milli** second
    printf("%.3f\t", speed);
  }

  printf("\n");
  return 0;
}
