/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
                      Daniel García-García  (UPM)
 *                    Pablo García-Hernández (UPM)
 *
 * This file is part of B-hashtree Library.
 *
 *     B-hashtree Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     B-hashtree Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with B-hashtree Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

import java.util.*;

public class Benchmark {

    final static Random random = new Random(121);

    public static ArrayList<Double> evaluate(Map<Integer, ItemValue> map, int size, double p, int numberOfOp) {
        ArrayList<Double> ellapsed = new ArrayList<>();
        ellapsed.add(System.nanoTime() / 1e6); // Take time in milliSeconds

        ItemValue[] values = new ItemValue[size];
        {
            int i = 0;
            while (i < size) {
                Integer key = random.nextInt(32 * size);
                if (!map.containsKey(key)) {
                    values[i] = new ItemValue(key);
                    map.put(key, values[i]);
                    ++i;
                }
            }
        }

        ellapsed.add(System.nanoTime() / 1e6); // Take time in milliSeconds

        // Get, Remove or Insert values
        Deque<ItemValue> deque = new ArrayDeque<>();
        final int dequeSizeLimit = 10;

        for (int i = 0; i < numberOfOp; ++i) {
            double chance = random.nextDouble();
            if (chance < p) { // Access
                ItemValue aValue = values[i % size];
                ItemValue iValue = map.get(aValue.key);
                if (iValue != null && !aValue.key.equals(iValue.key)) {
                    System.err.println("Keys should be equals. Should not be possible to get here");
                    System.exit(20);
                }
                if (iValue != null && !iValue.isInserted) {
                    System.err.println("Item should be inserted. Should not be possible to get here");
                    System.exit(21);
                }
                if (iValue != null) {
                    iValue.numberOfAccess += 1;
                } else {
                    aValue.numberOfNotFound += 1;
                }
            } else if (deque.size() <= dequeSizeLimit && (deque.size() == 0 || chance < p + (1 - p) / 2)) { // Remove
                int j = i;
                ItemValue aValue = values[j % size];
                while (!aValue.isInserted) {
                    j += 1;
                    aValue = values[j % size];
                }
                ItemValue iValue = map.remove(aValue.key);
                iValue.isInserted = false;
                iValue.numberOfRemovals += 1;
                deque.addLast(iValue);
            } else { // Insert
                ItemValue iValue = deque.removeFirst();
                iValue.numberOfInsertions += 1;
                iValue.isInserted = true;
                map.put(iValue.key, iValue);
            }
        }

        ellapsed.add(System.nanoTime() / 1e6); // Take time in milliSeconds

        int totalAccess = 0;
        int totalInsertions = 0;
        int totalRemovals = 0;

        // Checking operations
        for ( ItemValue aValue : values ) {
            numberOfOp -= aValue.numberOfAccess;
            numberOfOp -= aValue.numberOfInsertions;
            numberOfOp -= aValue.numberOfNotFound;
            numberOfOp -= aValue.numberOfRemovals;
            totalAccess += aValue.numberOfAccess + aValue.numberOfNotFound;
            totalInsertions += aValue.numberOfInsertions;
            totalRemovals += aValue.numberOfRemovals;
        }

        System.err.println(String.format("Access: %d, Insertions: %d, Removals: %d", totalAccess, totalInsertions, totalRemovals));

        if ( numberOfOp != 0 ) {
            System.err.println("NumberOfOp is not compute correctly. Should not be possible to get here");
            System.exit(22);
        }

        // ellapsed.add(System.nanoTime() / 1e6); // Take time in milliSeconds

        return ellapsed;
    }

    public static void main(String[] args) {
        int logSize = Integer.parseInt(args[1]);
        int size = 1 << logSize; // 2 ** logSize
        double p = Double.parseDouble(args[2]);
        int numberOfOp = Integer.parseInt(args[3]);
        ArrayList<Double> ellapsed = null;

        Map<Integer, ItemValue> map;
        if (args[0].equalsIgnoreCase("Hashtable")) {
            map = new Hashtable<>();
            ellapsed = evaluate(map, size, p, numberOfOp);
        } else if (args[0].equalsIgnoreCase("HashMap")) {
            map = new HashMap<>();
            ellapsed = evaluate(map, size, p, numberOfOp);
        } else if (args[0].equalsIgnoreCase("CuckooTable")) {
            map = new bhashtree.CuckooTable<>();
            ellapsed = evaluate(map, size, p, numberOfOp);
        }

        int[] numberofIterations = { size, numberOfOp };
        for (int i = 1; i < ellapsed.size(); ++i) {
            double speed = numberofIterations[i-1] / (ellapsed.get(i) - ellapsed.get(i - 1)); // number of iterations per **milli** second
            System.out.print(String.format(Locale.US,"%.3f\t", speed));
        }
        System.out.println();
    }

}

class ItemValue {

    public ItemValue(Integer key) {
        this.key = key;
    }

    /*
     * Si está o no insertado.
     * Las veces que se ha eliminado.
     * Las veces que se ha accedido a él.
     * Las veces que se ha insertado (después de la primera)
     */
    public Integer key;
    public boolean isInserted = true;
    public int numberOfRemovals = 0;
    public int numberOfAccess = 0;
    public int numberOfNotFound = 0;
    public int numberOfInsertions = 0;
}
