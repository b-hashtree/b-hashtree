
# Caso C

* En este caso se van a generar número aleatorios para las claves (0..32*Size),
luego vamos a comprobar que no estén repetidos hasta llegar a tamaño deseado.

* Vamos a meter como valor un objeto cuyo estado contiene varios atributos:

- Si está o no insertado.
- Las veces que se ha eliminado.
- Las veces que se ha accedido a él.
- Las veces que se ha insertado (después de la primera)

* Vamos a guardar una cola con elementos eliminados

* Vamos a hacer number_of_operations de operaciones entre las siguientes
posibilidades (porcentaje de probabilidad), vamos recorriendo el vector: 

a) Insertar / Eliminar. Si size(cola) > 10 siempre insertamos. Si size(cola) == 0
siempre eliminados. En otro caso, la mitad de insertar/eliminar. 
b) Acceder. Incrementar el valor de los accesos. 

Eliminar aplica al elemento con el índice que llevamos. Lo ponemos en la cola y lo 
marcamos como que no está insertado. 

Acceder también aplica al elemento. Comprobaremos que efectivamente está o no está
dependiendo de que se haya eliminado antes o no. 

Insertar se aplica al primer elemento de la fila. No aumentamos la j que marca 
por donde vamos. 