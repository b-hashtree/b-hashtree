""" ***************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
                      Daniel García-García  (UPM)
 *                    Pablo García-Hernández (UPM)
 *
 * This file is part of B-hashtree Library.
 *
 *     B-hashtree Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     B-hashtree Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with B-hashtree Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/ """

import sys
import time
import random
import collections
import os 

if __name__ == "__main__":
    dir_path = os.path.dirname(os.path.realpath(__file__))
    if os.getcwd() == dir_path:
        sys.path.append("../..")
    else:
        print("Error: this script is designed to be executed in his own folder as working dir")
        sys.exit(22)

from bhashtree.cuckoo_table import CuckooTable

class ItemValue:
    def __init__(self, key):
        self.key = key
        self.isInserted = True
        self.numberOfRemovals = 0
        self.numberOfAccess = 0
        self.numberOfNotFound = 0
        self.numberOfInsertions = 0

class CaseC:

    @staticmethod
    def evaluate(map, size, p, numberOfOp):
        ellapsed = [ time.time_ns()/1e6 ] # milliseconds

        values = [] 
        i = 0
        while i < size:
            key = random.randint(0, 32 * size)
            if not key in map:
                values.append(ItemValue(key))
                map[key] = values[i]
                i += 1
    
        ellapsed.append(time.time_ns()/1e6)

        # Get, Remove or Insert values
        deque = collections.deque()
        dequeSizeLimit = 10

        for i in range(0,numberOfOp):
            chance = random.uniform(0,1)
            if chance < p: # Access
                aValue = values[i % size]
                try: 
                    iValue = map[aValue.key]
                
                    if aValue.key != iValue.key:
                        sys.stderr.write("Keys should be equals. Should not be possible to get here\n")
                        exit(20)
                    if not iValue.isInserted:
                        sys.stderr.write("Item should be inserted. Should not be possible to get here\n")
                        exit(21)

                    iValue.numberOfAccess += 1
                except: 
                    aValue.numberOfNotFound += 1
                
            elif len(deque) <= dequeSizeLimit and (len(deque) == 0 or chance < p + (1 - p) / 2): # Remove
                j = i
                aValue = values[j % size]
                while not aValue.isInserted:
                    j += 1
                    aValue = values[j % size]
                
                iValue = map.pop(aValue.key)
                iValue.isInserted = False
                iValue.numberOfRemovals += 1
                deque.append(iValue)
            else: # Insert
                iValue = deque.popleft()
                iValue.numberOfInsertions += 1
                iValue.isInserted = True
                map[iValue.key] = iValue
    
        ellapsed.append(time.time_ns()/1e6)

        totalAccess = 0
        totalInsertions = 0
        totalRemovals = 0

        # Checking operations
        for aValue in values:
            numberOfOp -= aValue.numberOfAccess
            numberOfOp -= aValue.numberOfInsertions
            numberOfOp -= aValue.numberOfNotFound
            numberOfOp -= aValue.numberOfRemovals
            totalAccess += aValue.numberOfAccess + aValue.numberOfNotFound
            totalInsertions += aValue.numberOfInsertions
            totalRemovals += aValue.numberOfRemovals

        sys.stderr.write(f"Access: {totalAccess}, Insertions: {totalInsertions}, Removals: {totalRemovals}\n")

        if numberOfOp != 0:
            sys.stderr.write("NumberOfOp is not compute correctly. Should not be possible to get here\n")
            exit(22)
    
        #ellapsed.append(time.time_ns()/1e6)

        return ellapsed

    @staticmethod
    def main():
        logSize = int(sys.argv[1])
        size = 1 << logSize
        prob = float(sys.argv[2])
        numberOfOp = int(sys.argv[3])
        which = str(sys.argv[4])
        if which == "dict": map = {}
        elif which == "cuckoo-table": map = CuckooTable()
        else:
            sys.stderr.write("Unknown class: " + which + "\n")
            sys.exit(22)

        ellapsed = CaseC.evaluate(map, size, prob, numberOfOp)

        numberofIterations = [ size, numberOfOp] 
        for i in range(1,len(ellapsed)):
            speed = numberofIterations[i-1] / (ellapsed[i]-ellapsed[i-1])
            print(speed, end='\t')
        print()

if __name__ == "__main__":
    CaseC.main()
