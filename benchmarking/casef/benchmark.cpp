/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
                      Daniel García-García  (UPM)
 *                    Pablo García-Hernández (UPM)
 *
 * This file is part of B-hashtree Library.
 *
 *     B-hashtree Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     B-hashtree Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with B-hashtree Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/


#include <string>
#include <iostream>
#include <sstream>
#include <random>
#include <algorithm>
#include <iterator>
#include <chrono>
#include <cstdio>
#include <unordered_map>
#include <map>
#include <tuple>

#ifdef DEBUGGING_OUTPUTS
#define LOG_LEVEL 15
#else
#define LOG_LEVEL 0
#endif

#include "../shuffle_array.hpp"

#include "bhashtree/array_adapter.hpp"
#include "bhashtree/cuckoo_table.hpp"
#include "bhashtree/hamt.hpp"

using namespace std;

typedef pair<chrono::time_point<chrono::high_resolution_clock>, string> checkpoint_t;
typedef vector<checkpoint_t> checkpoints_t;

class TheValue 
{
  public:
    string nextKey;
    int counter = 0;
    int increment = 0;
    
    template<typename Map>
    TheValue(const string& previousKey, Map& map) {
        static auto hasher = hash<string>();
        int h = hasher(previousKey + "1234567890");
        nextKey = "A" + to_string(h);
        while ( map.count(nextKey) > 0 ) {
            increment += 1;
            nextKey = "A" + to_string(h) + "/" + to_string(increment);
        }
    }

    TheValue(const string& first) {
        nextKey = first;
    }
};

template <class table_t>
void case_f(size_t size, size_t numberOfOp, checkpoints_t &ellapsed)
{
  table_t map;

  ellapsed.push_back(checkpoint_t(chrono::high_resolution_clock::now(), "Start")); // Taking time

  // Build Map (checking duplicate in this benchmark)
  string first = "1";
  string key = first;

  for (size_t i = 0; i < size-1; ++i) {
      TheValue theValue(key, map);
      map.insert(typename table_t::value_type(key, theValue));
      key = theValue.nextKey;
  }

  map.insert(typename table_t::value_type(key, TheValue(first)));

  ellapsed.push_back(checkpoint_t(chrono::high_resolution_clock::now(), "Build Map")); // Taking time
  key = first;
  for (size_t j = 0; j < numberOfOp; ++j) {
      TheValue& theValue = map.at(key);
      theValue.counter += 1;
      key = theValue.nextKey;
  }

  ellapsed.push_back(checkpoint_t(chrono::high_resolution_clock::now(), "Lookups")); // Taking time
  size_t accessed = numberOfOp / size;
  key = first;
  for (size_t j = 0; j < size; ++j) {
      TheValue& theValue = map.at(key);
      if ( theValue.counter != accessed ) {
          cerr << "Bad number of access" << endl;
          exit(2);
      }
      key = theValue.nextKey;
  }
}

int main(int argc, char **argv)
{
  int exponent = 5;
  char which = 'c';
  size_t number_of_operations = 100;

  if (argc >= 2)
  {
    istringstream arg1(argv[1]);
    arg1 >> exponent;
  }

  if (argc >= 3)
  {
    istringstream arg2(argv[2]);
    arg2 >> which;
  }

  if (argc >= 4)
  {
    istringstream arg2(argv[3]);
    arg2 >> number_of_operations;
  }

  size_t n_erase, n_get, n_insert;
  checkpoints_t time_ellapseds;
  size_t size = 1 << exponent; // 2 ** exponent

  switch (which)
  {
  case 'm':
    case_f<map<string, TheValue>>(size, number_of_operations, time_ellapseds);
    break;
  case 'u':
    case_f<unordered_map<string, TheValue>>(size, number_of_operations, time_ellapseds);
    break;
  case 'h':
    case_f<hamt<string, TheValue>>(size, number_of_operations, time_ellapseds);
    break;
  case 'c':
    case_f<cuckoo_table<string, TheValue>>(size, number_of_operations, time_ellapseds);
    break;
  default:
    cerr << "Can't find class id for`" << which << "'" << endl;
    exit(21);
    break;
  }

  for (int i = 1; i < time_ellapseds.size(); ++i)
  {
    auto t = chrono::duration_cast<chrono::duration<double>>(time_ellapseds[i].first - time_ellapseds[i - 1].first); // seconds
    printf("%.3f\t", t.count() * 1e3);
  }
  auto t1 = chrono::duration_cast<chrono::duration<double>>(time_ellapseds[1].first - time_ellapseds[0].first); // seconds
  double building = size / t1.count() / 1e3;                                                                    // number of look-ups operations per **milli** second
  printf("%.3f\t", building);

  auto t2 = chrono::duration_cast<chrono::duration<double>>(time_ellapseds[2].first - time_ellapseds[1].first); // seconds
  double speed = number_of_operations / t2.count() / 1e3;                                                       // number of look-ups operations per **milli** second
  printf("%.3f", speed);

  printf("\n");
  return 0;
}

