""" ***************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
                      Daniel García-García  (UPM)
 *                    Pablo García-Hernández (UPM)
 *
 * This file is part of B-hashtree Library.
 *
 *     B-hashtree Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     B-hashtree Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with B-hashtree Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/ """

import time
import sys
import os 

if __name__ == "__main__":
    dir_path = os.path.dirname(os.path.realpath(__file__))
    if os.getcwd() == dir_path:
        sys.path.append("../..")
    else:
        print("Error: this script is designed to be executed in his own folder as working dir")
        sys.exit(22)

from bhashtree.cuckoo_table import CuckooTable

class TheValue: 

    @staticmethod
    def checkingNextKey(previousKey: str, map: dict):
        previousKey += "1234567890"
        h = hash(previousKey)
        increment = 0
        nextKey = "A" + str(h)
        while nextKey in map:
            increment += 1
            nextKey = "A" + str(h) + "/" + str(increment)
        return TheValue(nextKey, increment)

    def __init__(self, nextKey: str, increment: int = 0) :
        self.increment = increment
        self.nextKey = nextKey
        self.counter = 0

class CaseF:

    @staticmethod
    def evaluate(map, size, numberOfOp):
        ellapsed = [ time.time_ns()/1e6 ] # milliseconds

        first = "1"
        key = first

        for i in range(0, size-1) :
            theValue = TheValue.checkingNextKey(key, map)
            map[key] = theValue
            key = theValue.nextKey

        map[key] = TheValue(first)

        ellapsed.append(time.time_ns()/1e6)

        key = first
        for j in range(0, numberOfOp) : 
            theValue = map[key]
            theValue.counter += 1
            key = theValue.nextKey
    
        ellapsed.append(time.time_ns()/1e6)

        accessed = numberOfOp / size 
        key = first
        for j in range(0, numberOfOp) : 
            theValue = map[key]
            if theValue.counter != accessed:
                sys.stderr.write("Bad number of access")
                sys.exit(2)
            key = theValue.nextKey

        return ellapsed

    @staticmethod
    def main():
        logSize = int(sys.argv[1])
        size = 1 << logSize
        number_of_operations = int(sys.argv[2])
        which = str(sys.argv[3])
        if which == "dict": map = {}
        elif which == "cuckoo-table": map = CuckooTable()
        else:
            sys.stderr.write("Unknown class: " + which + "\n")
            sys.exit(22)

        ellapsed = CaseF.evaluate(map,size,number_of_operations)

        for i in range(1,len(ellapsed)):
            print(ellapsed[i]-ellapsed[i-1], end='\t')
        
        speed = size / (ellapsed[1] - ellapsed[0])
        print(speed, end='\t')
        
        speed = number_of_operations / (ellapsed[2] - ellapsed[1])
        print(speed)

if __name__ == "__main__":
    CaseF.main()
