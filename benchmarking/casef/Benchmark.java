/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
                      Daniel García-García  (UPM)
 *                    Pablo García-Hernández (UPM)
 *
 * This file is part of B-hashtree Library.
 *
 *     B-hashtree Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     B-hashtree Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with B-hashtree Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

import java.util.*;

public class Benchmark {

    final static Random random = new Random(30);

    public static ArrayList<Double> evaluateString(Map<String, TheValue> map, int size, int numberOfOp) {
        ArrayList<Double> ellapsed = new ArrayList<>();

        ellapsed.add(System.nanoTime() / 1e6); // Take time in milliSeconds
        String first = "1";
        String key = first;

        for (int i = 0; i < size-1; ++i) {
            TheValue theValue = new TheValue(key, map);
            map.put(key, theValue);
            key = theValue.nextKey;
        }

        map.put(key, new TheValue(first));

        ellapsed.add(System.nanoTime() / 1e6); // Take time in milliSeconds

        key = first;
        for (int j = 0; j < numberOfOp; ++j) {
            TheValue theValue = map.get(key);
            theValue.counter += 1;
            key = theValue.nextKey;
        }

        ellapsed.add(System.nanoTime() / 1e6); // Take time in milliSeconds

        int accessed = numberOfOp / size;
        key = first;
        for (int j = 0; j < size; ++j) {
            TheValue theValue = map.get(key);
            if ( theValue.counter != accessed ) {
                System.err.println("Bad number of access");
                System.exit(2);
            }
            key = theValue.nextKey;
        }

        return ellapsed;
    }

    public static void main(String[] args) {
        int logSize = Integer.parseInt(args[1]);
        int size = 1 << logSize; // 2 ** logSize
        int numberOfOp = Integer.parseInt(args[2]);
        ArrayList<Double> ellapsed = null;

        Map<String, TheValue> map;
        if (args[0].equalsIgnoreCase("Hashtable")) {
            map = new Hashtable<>();
            ellapsed = evaluateString(map, size, numberOfOp);
        } else if (args[0].equalsIgnoreCase("HashMap")) {
            map = new HashMap<>();
            ellapsed = evaluateString(map, size, numberOfOp);
        } else if (args[0].equalsIgnoreCase("CuckooTable")) {
            map = new bhashtree.CuckooTable<>();
            ellapsed = evaluateString(map, size, numberOfOp);
        } else {
            System.err.println("Unknown class codename");
            System.exit(3);
        }

        for (int i = 1; i < ellapsed.size(); ++i) {
            System.out.print(String.format(Locale.US, "%.3f\t", ellapsed.get(i) - ellapsed.get(i - 1)));
        }
        double buildSpeed = size / (ellapsed.get(1) - ellapsed.get(0));
        System.out.print(String.format(Locale.US, "%.3f\t", buildSpeed));
        double speed = numberOfOp / (ellapsed.get(2) - ellapsed.get(1));
        System.out.print(String.format(Locale.US, "%.3f", speed));
        System.out.println();
    }
}

class TheValue {
    public String nextKey;
    public int counter = 0;
    public int increment = 0;
    
    public TheValue(String previousKey, Map<String, TheValue> map) {
        previousKey += "1234567890";
        int h = previousKey.hashCode();
        nextKey = "A" + h;
        while ( map.containsKey(nextKey) ) {
            increment += 1;
            nextKey = "A" + h + "/" + increment;
        }
    }

    public TheValue(String first) {
        nextKey = first;
    }
}
