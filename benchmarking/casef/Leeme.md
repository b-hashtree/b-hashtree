
# Caso F

* El objetivo de este caso es probar un caso más o menos
parecido a las cadenas, pero que no haya aleatoriedad

* La clave va a ser una string y el valor otro string
que se obtiene a partir de convertir a string el hash
de la clave y modificarlo hasta que no exista. 

* El truco es que el valor será la clave del siguiente 
par de clave, valor

* Vamos a recorrer la "cadena" así formada contabilizando
los accesos y los comprobaremos al final en un último 
bucle. 
