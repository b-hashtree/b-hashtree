# /* ***************************************************************************
# * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
#                      Daniel García-García (UPM)
# *                    Pablo García-Hernández (UPM)
# *
# * This file is part of B-hashtree Library.
# *
# *     B-hashtree Library is free software: you can redistribute it and/or modify
# *     it under the terms of the GNU General Public License as published by
# *     the Free Software Foundation, either version 3 of the License, or
# *     (at your option) any later version.
# *
# *     B-hashtree Library is distributed in the hope that it will be useful,
# *     but WITHOUT ANY WARRANTY; without even the implied warranty of
# *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# *     GNU General Public License for more details.
# *
# *     You should have received a copy of the GNU General Public License
# *     along with B-hashtree Library.  If not, see <http://www.gnu.org/licenses/>.
# *****************************************************************************/

cmake_minimum_required(VERSION 3.16)

# This project is intented to run as subproject due to dependencies on the compiled programs
project(cased-benchmark) 

set(CaseId CaseD)

set(CppTarget CppBenchmark_${CaseId})
set(CppExecPath ${CMAKE_CURRENT_BINARY_DIR}/${CppTarget})
set(CMAKE_CXX_STANDARD 20)

set(JavaTarget JavaBenchmark_${CaseId})

set(CaseOutputDir ${CMAKE_SOURCE_DIR}/article/figures/${CaseId})

# include software root dir
include_directories(${CMAKE_SOURCE_DIR})

# CPP Program
add_executable(${CppTarget}
        benchmark.cpp
        ${CMAKE_SOURCE_DIR}/bhashtree/cuckoo_table.hpp
        ${CMAKE_SOURCE_DIR}/bhashtree/hamt.hpp)

# JAVA Program
find_package(Java REQUIRED)
include(UseJava)

# To introduce java compile flags (no need yet)
# set(CMAKE_JAVA_COMPILE_FLAGS "-source" "1.6" "-target" "1.6")
add_jar(${JavaTarget}
    Benchmark.java 
    ${CMAKE_SOURCE_DIR}/bhashtree/CuckooTable.java 
    ${CMAKE_SOURCE_DIR}/benchmarking/ShuffleArray.java)

get_target_property(_jarFile ${JavaTarget} JAR_FILE)
get_target_property(_classDir ${JavaTarget} CLASSDIR)

# message(STATUS "[${CaseId}] Cpp Target: ${CppTarget}")
message(STATUS "[${CaseId}] CppExecPath: ${CppExecPath}")
# message(STATUS "[${CaseId}] Java Target: ${JavaTarget}")
message(STATUS "[${CaseId}] Jar file ${_jarFile}")
# message(STATUS "Class compiled to ${_classDir}")

add_custom_target( generate_tsv_${CaseId}_cpp ALL
    DEPENDS ${CaseOutputDir}/cpp-output.tsv
)

add_custom_target( generate_tsv_${CaseId}_java ALL
    DEPENDS ${CaseOutputDir}/java-output.tsv
)

add_custom_target( generate_tsv_${CaseId}_python ALL
    DEPENDS ${CaseOutputDir}/python-output.tsv
)

add_custom_command(
    OUTPUT ${CaseOutputDir}/cpp-output.tsv
    COMMAND bash -c "mkdir -p ${CaseOutputDir}; cp *.tsv ${CaseOutputDir}/"
    WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
    COMMENT "Copying files to ${CaseOutputDir}/"
    DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/cpp-output.tsv
    VERBATIM
)

add_custom_command(
    OUTPUT ${CaseOutputDir}/java-output.tsv
    COMMAND bash -c "mkdir -p ${CaseOutputDir}; cp *.tsv ${CaseOutputDir}/"
    WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
    COMMENT "Copying files to ${CaseOutputDir}/"
    DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/java-output.tsv
    VERBATIM
)

add_custom_command(
    OUTPUT ${CaseOutputDir}/python-output.tsv
    COMMAND bash -c "mkdir -p ${CaseOutputDir}; cp *.tsv ${CaseOutputDir}/"
    WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
    COMMENT "Copying files to ${CaseOutputDir}/"
    DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/python-output.tsv
    VERBATIM
)

add_custom_command(
    OUTPUT ${CMAKE_CURRENT_SOURCE_DIR}/cpp-output.tsv
    COMMAND bash run_cases_cpp.sh ${CppExecPath} 
    WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
    COMMENT "[${CaseId}] Creating a TSV-CPP file to display the data in figures"
    DEPENDS ${CppTarget} ${CMAKE_CURRENT_SOURCE_DIR}/run_cases_cpp.sh
    VERBATIM
)

add_custom_command(
    OUTPUT ${CMAKE_CURRENT_SOURCE_DIR}/java-output.tsv
    COMMAND bash run_cases_java.sh ${_jarFile}
    WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
    COMMENT "[${CaseId}] Creating a TSV java file to display the data in figures"
    DEPENDS ${JavaTarget} ${CMAKE_CURRENT_SOURCE_DIR}/run_cases_java.sh
    VERBATIM
)

# PYTHON Assesment
add_custom_command(
    OUTPUT ${CMAKE_CURRENT_SOURCE_DIR}/python-output.tsv
    COMMAND bash run_cases_python.sh 
    WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
    COMMENT "[${CaseId}] Creating a TSV python file to display the data in figures"
    DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/benchmark.py ${CMAKE_CURRENT_SOURCE_DIR}/run_cases_python.sh
    VERBATIM
)
