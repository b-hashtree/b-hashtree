
# Caso D

* Vamos a probar un mapa con claves que son string, pero cuyo 
contenido lo vamos a generar como números aleatorios. 

* Para generar números aleatorios para las claves usaremos
un rango hasta 32*size, luego vamos a comprobar que no estén repetidos
(comprobando que no están en el map), e insertamos hasta llegar a tamaño deseado.

* Vamos a meter como valor un número real y luego los vamos a sumar
para sacar la media. Tiene que salir aproximadamente 0.5.

* No vamos a hacer ningún desplazamiento (shift).
