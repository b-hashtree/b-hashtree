package sandbox;

public class ShowHashes {
    public static void main(String[] args) {
        int bucketLogSize = 8;
        int shift = 5; 
        System.out.println("Integer Size: " + Integer.SIZE);
        for ( int i = 0; i < 100; ++i ) {
            Integer num = i << shift;
            int h = num.hashCode();
            int r1 = hash1(h, bucketLogSize);
            int r2 = hash2(h, bucketLogSize);
            System.out.println(String.format("i = %d, h = %d, hash1 = %d, hash2 = %d", i, h, r1, r2));
        }
    }

    static int hash1(int h, int bucketLogSize) {
        int result = h & 0x0f;
        h = h >> 8;
        for ( int i = 1; i < 4; ++i ) {
            result += (h & 0x0f) << (i * 4);
            h = h >> 8;
        }            
        return result & ((1 << bucketLogSize) - 1); // Only the mask to bucketLogSize
    }

    static int hash2(int h, int bucketLogSize) {
        int result = h & 0xf0;
        h = h >> 8;
        for ( int i = 1; i < 4; ++i ) {
            result += (h & 0xf0) << (i * 4);
            h = h >> 8;
        }            
        return result & ((1 << bucketLogSize) - 1); // Only the mask to bucketLogSize
    }

    static int hash3(int h, int bucketLogSize) {
        int result = h & 0xff;
        h = h >> 8;
        for ( int i = 1; i < 4; ++i ) {
            result ^= (h & 0xff) + 0x9e3779b9 + (result<<6) + (result>>2);
            h = h >> 8;
        }            
        return result & ((1 << bucketLogSize) - 1); // Only the mask to bucketLogSize
    }

    static int hash4(int h, int bucketLogSize) {
        int result = h & 0xff;
        h = h >> 8;
        for (int j = 0; j < 3; ++j) {
            result = result * 31 + (h & 0xff);
            h = h >> 8;
        }
        return result & ((1 << bucketLogSize) - 1); // Only the mask to bucketLogSize
    }
}
