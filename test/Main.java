
package test;

import java.util.*;

public class Main {

    final static Random random = new Random(121);

    public static ArrayList<Double> evaluateStr(Map<String, Integer> map, int logSize, int shift) {
        ArrayList<Double> ellapsed = new ArrayList<>();

        ellapsed.add(System.nanoTime() / 1e6); // Take time in milliSeconds

        int size = 1 << logSize; // 2 ** logSize
        long[] numbers = new long[size];
        for (int i = 0; i < size; ++i) {
            // numbers[i] = random.nextInt(50 * size) << shift;
            numbers[i] = random.nextLong() << shift;
        }

        ellapsed.add(System.nanoTime() / 1e6); // Take time in milliSeconds

        // Build Map
        for (int i = 0; i < size; ++i) {
            String key = String.format("%o", numbers[i]); // o: Octal (digits 0..7)
            Integer previous = map.put(key, i);
            if (previous != null) {
                numbers[previous] = -1;
            }
        }

        ellapsed.add(System.nanoTime() / 1e6); // Take time in milliSeconds

        // Get values
        for (int i = 0; i < size; ++i) {
            if (numbers[i] != -1) {
                String key = String.format("%o", numbers[i]); // o: Octal (digits 0..7)
                Integer index = map.get(key);
                if (index != null) {
                    if (index != i) {
                        System.out.println(String.format("Error: incorrect value", key));
                        System.exit(1);
                    }
                } else {
                    System.out.println(String.format("Error: key `%s` not found in index %d", key, i));
                    System.exit(1);
                }
            }
        }

        ellapsed.add(System.nanoTime() / 1e6); // Take time in milliSeconds

        // Remove some (1/4) keys without counting duplicates
        int numberOfRemovals = size / 4;
        for (int i = 0; i < numberOfRemovals; ++i) {
            if (numbers[i] != -1) {
                String key = String.format("%o", numbers[i]); // o: Octal (digits 0..7)
                Integer index = map.remove(key);
                if (index != null) {
                    long value = numbers[index];
                    if (value != numbers[i]) {
                        System.out.println(String.format("Error: incorrect value in removing", key));
                        System.exit(1);
                    }
                } else {
                    System.out.println(String.format("Error: key `%s` not found in index %d", key, i));
                    System.exit(1);
                }
            }
        }

        ellapsed.add(System.nanoTime() / 1e6); // Take time in milliSeconds

        // Check element doesn't exist
        for (int i = 0; i < numberOfRemovals; ++i) {
            if (numbers[i] != -1) {
                String key = String.format("%o", numbers[i]); // o: Octal (digits 0..7)
                Integer index = map.get(key);
                if (index != null) {
                    System.out.println(String.format("Error: key `%s` removed in index %d", key, i));
                    System.exit(1);
                }
            }
        }
        ellapsed.add(System.nanoTime() / 1e6); // Take time in milliSeconds
        return ellapsed;
    }

    public static ArrayList<Double> evaluateInt(Map<Long, Integer> map, int logSize, int shift) {
        ArrayList<Double> ellapsed = new ArrayList<>();

        ellapsed.add(System.nanoTime() / 1e6); // Take time in milliSeconds

        int size = 1 << logSize; // 2 ** logSize
        long[] numbers = new long[size];
        for (int i = 0; i < size; ++i) {
            // numbers[i] = random.nextInt(50 * size) << shift;
            numbers[i] = random.nextLong() << shift;
        }

        ellapsed.add(System.nanoTime() / 1e6); // Take time in milliSeconds

        // Build Map
        for (int i = 0; i < size; ++i) {
            Long key = numbers[i];
            Integer previous = map.put(key, i);
            if (previous != null) {
                numbers[previous] = -1;
            }
        }

        ellapsed.add(System.nanoTime() / 1e6); // Take time in milliSeconds

        // Get values
        for (int i = 0; i < size; ++i) {
            if (numbers[i] != -1) {
                Long key = numbers[i];
                Integer index = map.get(key);
                if (index != null) {
                    if (index != i) {
                        System.out.println(String.format("Error: incorrect value", key));
                        System.exit(1);
                    }
                } else {
                    System.out.println(String.format("Error: key `%s` not found in index %d", key, i));
                    System.exit(1);
                }
            }
        }

        ellapsed.add(System.nanoTime() / 1e6); // Take time in milliSeconds

        // Remove some (1/4) keys without counting duplicates
        int numberOfRemovals = size / 4;
        for (int i = 0; i < numberOfRemovals; ++i) {
            if (numbers[i] != -1) {
                Long key = numbers[i];
                Integer index = map.remove(key);
                if (index != null) {
                    long value = numbers[index];
                    if (value != numbers[i]) {
                        System.out.println(String.format("Error: incorrect value in removing", key));
                        System.exit(1);
                    }
                } else {
                    System.out.println(String.format("Error: key `%s` not found in index %d", key, i));
                    System.exit(1);
                }
            }
        }

        ellapsed.add(System.nanoTime() / 1e6); // Take time in milliSeconds

        // Check element doesn't exist
        for (int i = 0; i < numberOfRemovals; ++i) {
            if (numbers[i] != -1) {
                Long key = numbers[i];
                Integer index = map.get(key);
                if (index != null) {
                    System.out.println(String.format("Error: key `%s` removed in index %d", key, i));
                    System.exit(1);
                }
                index = map.remove(key);
                if (index != null) {
                    System.out.println(String.format("Error: key `%s` already removed in index %d", key, i));
                    System.exit(1);
                }
            }
        }
        ellapsed.add(System.nanoTime() / 1e6); // Take time in milliSeconds
        return ellapsed;
    }

    public static void main(String[] args) {
        int logSize = Integer.parseInt(args[1]);
        int shift = Integer.parseInt(args[2]);
        ArrayList<Double> ellapsed = null;
        int mapSize = 0;

        if (args[3].equalsIgnoreCase("Integer")) {
            Map<Long, Integer> map;
            if (args[0].equalsIgnoreCase("Hashtable")) {
                map = new Hashtable<>();
                ellapsed = evaluateInt(map, logSize, shift);
                mapSize = map.size();
            } else if (args[0].equalsIgnoreCase("HashMap")) {
                map = new HashMap<>();
                ellapsed = evaluateInt(map, logSize, shift);
                mapSize = map.size();
            } else if (args[0].equalsIgnoreCase("CuckooTable")) {
                map = new bhashtree.CuckooTable<>();
                ellapsed = evaluateInt(map, logSize, shift);
                mapSize = map.size();
            }
        } else if (args[3].equalsIgnoreCase("String")) {
            Map<String, Integer> map;
            if (args[0].equalsIgnoreCase("Hashtable")) {
                map = new Hashtable<>();
                ellapsed = evaluateStr(map, logSize, shift);
                mapSize = map.size();
            } else if (args[0].equalsIgnoreCase("HashMap")) {
                map = new HashMap<>();
                ellapsed = evaluateStr(map, logSize, shift);
                mapSize = map.size();
            } else if (args[0].equalsIgnoreCase("CuckooTable")) {
                map = new bhashtree.CuckooTable<>();
                ellapsed = evaluateStr(map, logSize, shift);
                mapSize = map.size();
            }
        }

        System.out.print(
                String.format("%20s%15d%15d%10d", args[0] + "-" + args[3], mapSize, (1 << logSize) - mapSize, shift));
        for (int i = 1; i < ellapsed.size(); ++i) {
            System.out.print(String.format("%20.3f", ellapsed.get(i) - ellapsed.get(i - 1)));
        }
        System.out.println();
    }

}
