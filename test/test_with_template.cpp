
#include <string>
#include <iostream>
#include <sstream>
#include <random>
#include <algorithm>
#include <iterator>
#include <chrono>
#include <cstdio>
#include <unordered_map>
#include <map>

#ifdef DEBUGGING_OUTPUTS
#define LOG_LEVEL 15
#else
#define LOG_LEVEL 0
#endif

#include "bhashtree/cuckoo_table.hpp"
#include "bhashtree/hamt.hpp"
#include "bhashtree/hashmap_tree.hpp"

using namespace std;

template <class table_t>
inline void check_size(const table_t& table, unsigned i, const typename table_t::key_type& key, size_t duplicates, size_t expected_size, const string& at) {
    if ( table.size() != expected_size ) {
      cerr << "---> " << at << endl;
      cerr << "This is (key=" << key << ", i=" << i << ")" << endl;
      cerr << "Size: " << table.size() << " should be " << expected_size << " Duplicates: " << duplicates << endl;
      cerr << endl;
    }
}

typedef pair<chrono::time_point<chrono::high_resolution_clock>, string> checkpoint_t;
typedef vector<checkpoint_t> checkpoints_t;

template <class table_t, typename convert2key>
int full_test(size_t a_size, unsigned shift, checkpoints_t& ellapsed, size_t& table_size, size_t& duplicates) {
  ellapsed.push_back(checkpoint_t(chrono::high_resolution_clock::now(),"Start")); // Taking time

  unsigned seed = 30;
  default_random_engine random_eng(seed);

  uniform_int_distribution<unsigned> uniform_dist(0, 8*a_size);

  vector<int> numbers(a_size, -1);

  table_t table;

#if LOG_LEVEL > 1
  cerr << "Array Size: " << a_size << endl;
#endif

  // Generate random vector
  for ( unsigned i = 0; i < a_size; ++i ) {
    numbers[i] = uniform_dist(random_eng) << shift;
  }

  ellapsed.push_back(checkpoint_t(chrono::high_resolution_clock::now(),"Random Vector")); // Taking time

#if LOG_LEVEL > 10
  cerr << "Original random vector" << endl;
  copy( numbers.begin(), numbers.end(), ostream_iterator<int>(cerr," "));
  cerr << endl;
#endif

  // Build Map
  duplicates = 0;
  for ( unsigned i = 0; i < a_size; ++i ) {
    typename table_t::key_type key = convert2key(numbers[i]);
    if ( table.count(key) == 1 ) {
      ++duplicates;
      int previous = table.at(key);
      numbers[previous] = -1;
      if ( ! table.erase( key ) ) {
        cerr << "Error: could not remove value for key : `" << key << "`" << endl;
        return 1;
      }
      check_size<table_t>(table, i, key, duplicates, i - duplicates, "Error after removing duplicate");
    }
    table.insert(typename table_t::value_type(key, i));
    check_size<table_t>(table, i, key, duplicates, i + 1 - duplicates,"Error after inserting");
  }

  ellapsed.push_back(checkpoint_t(chrono::high_resolution_clock::now(), "Build Map")); // Taking time

  table_size = table.size();

#if LOG_LEVEL > 10
  cerr << "Modified random vector" << endl;
  copy( numbers.begin(), numbers.end(), ostream_iterator<int>(cerr," "));
  cerr << endl;
#endif

  // Check put and get (when the key exists)
  for ( unsigned i = 0; i < a_size; ++i ) {
    if ( numbers[i] != -1 ) {
      typename table_t::key_type key = convert2key(numbers[i]);
#if LOG_LEVEL > 30
      cerr << "Looking for key: " << key << endl;
#endif
      int index = table.at(key);
      if ( index != (int)i ) {
        cerr << "Error: incorrect value for key : `" << key << "`" << endl;
        cerr << "       should be " << i << " and " << index << " was found" << endl;
        return 1;
      }
    }
  }
  ellapsed.push_back(checkpoint_t(chrono::high_resolution_clock::now(), "Lookups")); // Taking time
  return 0;
}

int main(int argc, char** argv) {

  int exponent = 5;
  char which = 'c';
  unsigned shift = 0;

  if ( argc >= 2 ) {
    istringstream arg1(argv[1]);
    arg1 >> exponent;
  }

  if ( argc >= 3 ) {
    istringstream arg2(argv[2]);
    arg2 >> which;
  }

  if ( argc >= 4 ) {
    istringstream arg2(argv[3]);
    arg2 >> shift;
  }

  int returning = 0;
  checkpoints_t time_ellapseds;
  size_t table_size, duplicates;
  size_t a_size = 1 << exponent; // 2 ** exponent  

  switch ( which ) {
    case 'm':
      cout << "std::map" << "\t";
      returning = full_test< map<unsigned, unsigned>, unsigned >(a_size, shift, time_ellapseds, table_size, duplicates);
      break;
    case 'u':
      cout << "std::unordered_map" << "\t";
      returning = full_test< unordered_map<unsigned, unsigned>, unsigned >(a_size, shift, time_ellapseds, table_size, duplicates);
      break;
    case 'h':
      cout << "Hamt" << "\t";
      returning = full_test< hamt<unsigned, unsigned>, unsigned >(a_size, shift, time_ellapseds, table_size, duplicates);
      break;
    case 'c':
      cout << "Cuckoo_Table" << "\t";
      returning = full_test< cuckoo_table<unsigned, unsigned>, unsigned  >(a_size, shift, time_ellapseds, table_size, duplicates);
      break;
    case 'b':
      cout << "Hashmap_tree" << "\t";
      returning = full_test< hashmap_tree<unsigned, unsigned>, unsigned  >(a_size, shift, time_ellapseds, table_size, duplicates);
      break;
    default:
      cout << "Unknown key for mapping class" << endl;
      return 31;
  }

  printf("%d\t", shift);
  printf("%ld\t", a_size);
  printf("%ld\t", table_size);
  printf("%ld\t", duplicates);
  vector<double> seconds(time_ellapseds.size()-1);
  for ( int i = 1; i < time_ellapseds.size(); ++i ) {
    auto t = chrono::duration_cast<chrono::duration<double>>(time_ellapseds[i].first - time_ellapseds[i-1].first); // seconds
    seconds.push_back(t.count());
    printf("%.3f\t", t.count() * 1e3);
  }
  double total_seconds = 0;
  for ( size_t i = 1; i < seconds.size(); ++i ) { // Skip random vector
    total_seconds += seconds[i];
  }
  double speed = 1e-3 * 2 * a_size / total_seconds; //approximate number of operations per **milli** second
  printf("%.3f", speed);

  printf("\n");
  return returning;
}
